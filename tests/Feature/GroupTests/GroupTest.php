<?php

namespace Tests\Feature\GroupTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Group;
use App\Student;

class GroupTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function group_returns_correct_number_of_students()
    {
        $group = Group::find(1);
        $studentsInGroup = Student::where('group_id', $group->group_id);
        
        $this->assertEquals($group->students->count(), $studentsInGroup->count());
    }
    
    /** @test */
    public function group_correctly_updates_after_new_student_is_added()
    {
        // initial
        $group = Group::find(1);
        $currStudentsInGroup = Student::where('group_id', $group->group_id);
        $currStudCount = $group->students->count();
        
        // action
        $newStudent = Student::create([
            'department_id' => 1,
            'group_id' => 1,
            'first_name' => 'Test',
            'last_name' => 'Student',
            'email' => 'test@gmail.com',
            'username' => 'tests',
            'password' => bcrypt('secret'),
            'gender' => '0',
            'id_card' => '0114801B',
            'remember_token' => str_random(10)
        ]);
        
        // expected results
        $group = Group::find(1);
        $newCount = $group->students->count();
        $newStudentInGroupCnt = Student::where('group_id', $group->group_id)->count();
        
        // assertions
        $this->assertEquals($newCount, $newStudentInGroupCnt);
        $this->assertNotEquals($currStudCount, $newCount);
    }
}
