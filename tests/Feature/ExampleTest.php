<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Group;
use App\Student;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;
    
    public function testBasicTest()
    {
        // given
        $group = Group::find(1);
        $studentsInGroup = Student::where('group_id', $group->group_id)->count();
        
        // expected
        $this->assertEquals($group->students->count(), $studentsInGroup);
    }
}
