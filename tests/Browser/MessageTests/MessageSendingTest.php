<?php

namespace Tests\Browser\MessageTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\Manager;
use App\Message;

class MessageSendingTest extends DuskTestCase
{
    /**
     * Test Case ID - 34
     *
     * Title - Director can send message to manager
     * 
     * Purpose - This test case asserts that director can send message to the manager and that manager will see the message.
     * After sending message, and asserting that manager sees it, test removes the last message since it is testing message.
     * 
     * @return void
     */
    public function test_director_can_send_message()
    {
        $this->browse(function ($directorBrowser, $managerBrowser) {
            $directorBrowser->loginAs(Director::find(1), 'director')
                    ->visit('/director/conversations')
                    ->assertSee('Filip Djuricic')
                    ->visit('/director/conversations/1')
                    ->type('reply_msg', 'Test Message')
                    ->press('Reply');
            
            $lastMessage = Message::where('conversation_id', 1)->orderBy('created_at', 'desc')->first();
            
            $managerBrowser->loginAs(Manager::find(1), 'manager')
                           ->visit('/manager/conversations')
                           ->assertSee(Director::find(1)->first_name.' '.Director::find(1)->last_name)
                           ->assertSee($lastMessage->message_content);
            
            $lastMessage->delete();
        });
    }

    /**
     * Test Case ID - 35
     *
     * Title - Manager can send message to director
     *
     * Purpose - This test case asserts that manager can send message to the director and that director will see the message.
     * After sending message, and asserting that director sees it, test removes the last message since it is testing message.
     *
     * @return void
     */
    public function test_manager_can_send_message()
    {
        $this->browse(function ($managerBrowser, $directorBrowser) {
            $managerBrowser->loginAs(Manager::find(1), 'manager')
                ->visit('/manager/conversations')
                ->assertSee('Joe Borg')
                ->type('reply_msg', 'Test Reply')
                ->press('Reply');

            $lastMessage = Message::where('conversation_id', 1)->orderBy('created_at', 'desc')->first();

            $directorBrowser->loginAs(Director::find(1), 'director')
                ->visit('/director/conversations')
                ->assertSee('Filip Djuricic')
                ->visit('/director/conversations/1')
                ->assertSee($lastMessage->message_content);
            
            $lastMessage->delete();
        });
    }
}
