<?php

namespace Tests\Browser\DirectorTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\Manager;
use App\Demand;

class DirectorRequestManagementTest extends DuskTestCase
{
    /**
        * Test Case ID - 28
        *
        * Title  - Director can't access request route if is innaccessible (solved already) or if it doesn't exist.
        *
        * Purpose - This test case asserts that director can't access the request if it is already solved (either approved or rejected)
        * or if it doesn't exist.
        * This is simply accomplished by trying to access unexisting request and asserting that director is redirected back.
        *
        * @return void
    */ 
    public function test_director_make_request_route_is_innaccessible_if_unexisting_request() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->visit('/director/request_more/unexisting_request')
                    // user returned back
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 29
        *
        * Title  - Director can read more about the request.
        *
        * Purpose - This test case asserts that director can read more about request made by manager.
        * This is simply accomplished by first creating test request and then trying to access it as director
        * and asserting that we see request data on the screen in appropriate sleectors (.inbox-body refers to class inbox-body, etc.).
        *
        * @return void
    */ 
    public function test_director_can_read_more_about_request_made_by_manager() 
    {
        $this->browse(function ($managerBrowser, $directorBrowser) {
                 $managerBrowser->loginAs(Manager::find(1), 'manager')
                                ->visit('/manager/make_request')
                                ->type('request_title', 'Test Request')
                                ->type('request_body', 'This request is testing request. Will be deleted after test.')
                                ->press('Make Request')
                                ->assertPathIs('/manager/browse_requests')
                                ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                                ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                                ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name);
                $directorBrowser->loginAs(Director::find(1), 'director')
                                ->visit('/director/browse_requests')
                                ->assertPathIs('/director/browse_requests')
                                ->assertSeeIn('#demand_table', Manager::find(Demand::all()->last()->manager_id)->first_name.' '.Manager::find(Demand::all()->last()->manager_id)->last_name)
                                ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                                ->visit(route('director.requests.read_more', ['request_id' => Demand::all()->last()->demand_id]))
                                ->assertPathIs('/director/request_more/'.Demand::all()->last()->demand_id)
                                ->assertSeeIn('.inbox-body', Demand::all()->last()->request_title)
                                ->assertSeeIn('.inbox-body', Demand::all()->last()->request_body)
                                ->assertSeeIn('.inbox-body', Manager::find(1)->email);
                // dispose last demand
                Demand::destroy(Demand::all()->last()->demand_id);
        });
    }
    
    /**
        * Test Case ID - 30
        *
        * Title  - Director can approve request and it will disappear from request table.
        *
        * Purpose - This test case asserts that director approve request and that it will no longer be visible to him (intended behaviour).
        * This is simply accomplished by first making test request as a manager and then loging in as director and approving that request,
        * and asserting that we no longer see it in the table.
        *
        * In order for this test to pass there needs to be at least 2 demands made by managers in demands table.
        * Otherwise it will display message that there are currently no unsolved requests (intended behaviour).
        *
        * @return void
    */
    public function test_director_can_approve_requests_and_after_they_will_disappear_from_request_table() 
    {
        $this->browse(function ($managerBrowser, $directorBrowser) {
            $managerBrowser->loginAs(Manager::find(1), 'manager')
                           ->visit('/manager/make_request')
                           ->type('request_title', 'Test Request')
                           ->type('request_body', 'This request is testing request. Will be deleted after test.')
                           ->press('Make Request')
                           ->assertPathIs('/manager/browse_requests')
                           ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                           ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                           ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name);
           $directorBrowser->loginAs(Director::find(1), 'director')
                           ->visit('/director/browse_requests')
                           ->assertPathIs('/director/browse_requests')
                           ->assertSeeIn('#demand_table', Manager::find(Demand::all()->last()->manager_id)->first_name.' '.Manager::find(Demand::all()->last()->manager_id)->last_name)
                           ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                           ->visit(route('director.requests.read_more', ['request_id' => Demand::all()->last()->demand_id]))
                           ->assertPathIs('/director/request_more/'.Demand::all()->last()->demand_id)
                           ->assertSeeIn('.inbox-body', Demand::all()->last()->request_title)
                           ->assertSeeIn('.inbox-body', Demand::all()->last()->request_body)
                           ->assertSeeIn('.inbox-body', Manager::find(1)->email)
                           ->press('Approve')
                           ->assertPathIs('/director/browse_requests');
                           
           // dispose last demand
           Demand::destroy(Demand::all()->last()->demand_id);
        });
    }
    
    /**
        * Test Case ID - 31
        *
        * Title  - Director can reject request and it will disappear from request table.
        *
        * Purpose - This test case asserts that director reject request and that it will no longer be visible to him (intended behaviour).
        * This is simply accomplished by first making test request as a manager and then loging in as director and rejecting that request,
        * and asserting that we no longer see it in the table.
        *
        * In order for this test to pass there needs to be at least 2 demands made by managers in demands table.
        * Otherwise it will display message that there are currently no unsolved requests (intended behaviour).
        *
        * @return void
    */
    public function test_director_can_reject_requests_and_after_they_will_disappear_from_request_table() 
    {
        $this->browse(function ($managerBrowser, $directorBrowser) {
            $managerBrowser->loginAs(Manager::find(1), 'manager')
                           ->visit('/manager/make_request')
                           ->type('request_title', 'Test Request')
                           ->type('request_body', 'This request is testing request. Will be deleted after test.')
                           ->press('Make Request')
                           ->assertPathIs('/manager/browse_requests')
                           ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                           ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                           ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name);
           $directorBrowser->loginAs(Director::find(1), 'director')
                           ->visit('/director/browse_requests')
                           ->assertPathIs('/director/browse_requests')
                           ->assertSeeIn('#demand_table', Manager::find(Demand::all()->last()->manager_id)->first_name.' '.Manager::find(Demand::all()->last()->manager_id)->last_name)
                           ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                           ->visit(route('director.requests.read_more', ['request_id' => Demand::all()->last()->demand_id]))
                           ->assertPathIs('/director/request_more/'.Demand::all()->last()->demand_id)
                           ->assertSeeIn('.inbox-body', Demand::all()->last()->request_title)
                           ->assertSeeIn('.inbox-body', Demand::all()->last()->request_body)
                           ->assertSeeIn('.inbox-body', Manager::find(1)->email)
                           ->press('Reject')
                           ->assertPathIs('/director/browse_requests');
                           
           // dispose last demand
           Demand::destroy(Demand::all()->last()->demand_id);
        });
    }
}
