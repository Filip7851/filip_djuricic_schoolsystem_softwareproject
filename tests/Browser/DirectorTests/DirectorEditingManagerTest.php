<?php

namespace Tests\Browser\DirectorTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\manager;

class DirectorEditingManagerTest extends DuskTestCase
{
    /**
        * Test Case ID - 33
        *
        * Title  - Director can edit manager data in the system.
        *
        * Purpose - This test case asserts that director can edit manager data in the system.
        * This is simply accomplished by first retriving manager that needs to be edited, saving it as reference
        * running the test, and later re-creating that manager.
        *
        * @return void
    */
    public function test_director_can_update_manager_profile()
    {
        $this->browse(function (Browser $browser) {
            // drop networks manager
            $manager = Manager::where('department_id', 2)->get()->first();
            $manRef = $manager;
            
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/modify_manager/'.$manager->id)
                    ->assertValue('#first_name', $manager->first_name)
                    ->assertValue('#last_name', $manager->last_name)
                    ->assertValue('#email', $manager->email)
                    ->assertValue('#id_card', $manager->id_card)
                    ->assertSeeIn('#departments', $manager->department->department_name)
                    ->type('first_name', $manager->first_name.'TESTZZ')
                    ->press('Modify')
                    ->assertSeeIn('#datatable', $manager->first_name);
            $manager->delete();
            
            Manager::create([
                'department_id' => $manRef->department_id,
                'first_name' => $manRef->first_name,
                'last_name' => $manRef->last_name,
                'email' => $manRef->email,
                'id_card' => $manRef->id_card,
                'username' => $manRef->username,
                'password' => $manRef->password,
            ]);
        });
    }
}
