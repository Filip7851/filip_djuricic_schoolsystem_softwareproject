<?php

namespace Tests\Browser\DirectorTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;

class DirectorRoutingTests extends DuskTestCase
{
    /**
        * Test Case ID - 1
        *
        * Title  - Director can't access login page after login 
        *
        * Purpose - This test case ensures that director auth guard cannot access the login page
        * after already logging in.
        * This is accomplished by first logging in director and then trying to access the login page again
        * and asserting that director is redirected back to the previous page.
        *
        * @return void
    */
    public function test_director_cant_access_director_login_page() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/director/login')
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 2
        *
        * Title  - Director can't access lecturer login page
        *
        * Purpose - This test case ensures that director auth guard cannot access the lecturer login page
        * after already logging in.
        * This is accomplished by first logging in director and then trying to access the lecturer login page
        * and asserting that director is redirected back to the previous page.
        *
        * @return void
    */
    public function test_director_cant_access_lecturer_login_page() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/lecturer/login')
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 3
        *
        * Title  - Director can't access student login page
        *
        * Purpose - This test case ensures that director auth guard cannot access the student login page
        * after already logging in.
        * This is accomplished by first logging in director and then trying to access the student login page
        * and asserting that director is redirected back to the previous page.
        *
        * @return void
    */
    public function test_director_cant_access_student_login_page() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/student/login')
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 4
        *
        * Title  - Director can't access manager login page
        *
        * Purpose - This test case ensures that director auth guard cannot access the manager login page
        * after already logging in.
        * This is accomplished by first logging in director and then trying to access the manager login page
        * and asserting that director is redirected back to the previous page.
        *
        * @return void
    */
    public function test_director_cant_access_manager_login_page() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/manager/login')
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 5
        *
        * Title  - Director can't access manager routes
        *
        * Purpose - This test case ensures that director auth guard cannot access the manager-specific routes.
        *
        * This is accomplished by first logging in director and then trying to access the manager-specific route
        * and asserting that director is redirected back to the previous page.
        * Testing only on one route such as /manager/dashboard is sufficient since same middleware is applied
        * to all routes within ManagerController so it essentially protects all routes.
        *
        * @return void
    */
    public function test_director_cant_access_manager_routes() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/manager/dashboard')
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 6
        *
        * Title  - Director can't access lecturer routes
        *
        * Purpose - This test case ensures that director auth guard cannot access the lecturer-specific routes.
        *
        * This is accomplished by first logging in director and then trying to access the lecturer-specific route
        * and asserting that director is redirected back to the previous page.
        * Testing only on one route such as /lecturer/dashboard is sufficient since same middleware is applied
        * to all routes within LecturerController so it essentially protects all routes.
        *
        * @return void
    */
    public function test_director_cant_access_lecturer_routes() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/lecturer/dashboard')
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 7
        *
        * Title  - Director can't access student routes
        *
        * Purpose - This test case ensures that director auth guard cannot access the student-specific routes.
        *
        * This is accomplished by first logging in director and then trying to access the student-specific route
        * and asserting that director is redirected back to the previous page.
        * Testing only on one route such as /student/dashboard is sufficient since same middleware is applied
        * to all routes within StudentController so it essentially protects all routes.
        *
        * @return void
    */
    public function test_director_cant_access_student_routes() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/student/dashboard')
                    ->assertPathIs('/director/dashboard');
        });
    }
    
    /**
        * Test Case ID - 8
        *
        * Title  - Director receives 404 page if accessing unexisting route.
        *
        * Purpose - This test case ensures that director auth guard receives 404 error informing him that
        *           he is trying to access non-existing route.
        *
        * This is accomplished by first logging in director and then trying to access the non-existing route
        * and asserting that director is shown 404 page.
        *
        * @return void
    */
    public function test_director_404_page_shows() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/unexisting_page/')
                    ->assertSee('404');
        });
    }
}
