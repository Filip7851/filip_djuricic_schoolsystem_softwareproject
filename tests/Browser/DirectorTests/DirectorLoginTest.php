<?php

namespace Tests\Browser\DirectorTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

use App\Director;

class DirectorLoginTest extends DuskTestCase
{
    /**
        * Test Case ID - 9
        *
        * Title  - Director dashboard can't be accessed before login.
        *
        * Purpose - This test case ensures that guest (non-authenticated) users can't access director
        *           dashboard before logging in.
        * This is accomplished by trying to visit /director/dashboard without authenticating.
        * We assert that we are redirected back to the original route /director/login.
        *
        * @return void
    */
    public function test_director_dashboard_cant_be_accessed_before_login() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/director/login')
                    ->assertSee('Director Login')
                    ->visit('/director/dashboard')
                    // returned back
                    ->assertPathIs('/director/login');
        });
    }
    
    /**
        * Test Case ID - 10
        *
        * Title  - Director login page is accessible
        *
        * Purpose - This test case asserts that users can access director login page.
        * This is simply accomplished by accessing /director/login without logging in.
        *
        * @return void
    */
    public function test_director_login_page_is_accessible()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/director/login')
                    ->assertSee('Director Login');
        });
    }
    
    /**
        * Test Case ID - 11
        *
        * Title  - Director's login page form validation performs correctly
        *
        * Purpose - This test case ensures that form validation on director login page performs correctly.
        * This is accomplished by first visiting /director/login and trying to submit form without
        * inputing any value in the text field and asserting that we see error messages.
        *
        * @return void
    */
    public function test_director_login_page_form_validation_performs_correctly()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/director/login')
                    ->type('username', '')
                    ->type('password', '')
                    ->press('Login')
                    ->assertSee('Please enter your username to proceed')
                    ->assertSee('Please enter your password to proceed');
        });
    }
    
    /**
        * Test Case ID - 12
        *
        * Title  - Director's login page form validation works on single fields.
        *
        * Purpose - This test case ensures that form validation on director login page performs correctly
        *           if only single fields contain error such as missing input.
        * This is accomplished by first visiting /director/login and trying to submit form with one field
        * such as username filled and one field such as password empty and vice-versa.
        * After that we assert that we see error messages.
        *
        * @return void
    */
    public function test_director_login_page_form_validation_single_fields() 
    {
        $this->browse(function ($first, $second) {
            $first->visit('/director/login')
                    ->type('username', '')
                    ->type('password', 'test')
                    ->press('Login')
                    ->assertSee('Please enter your username to proceed')
                    ->assertDontSee('Please enter your password to proceed');
            $second->visit('/director/login')
                   ->type('username', 'director')
                   ->type('password', '')
                   ->press('Login')
                   ->assertDontSee('Please enter your username to proceed')
                   ->assertSee('Please enter your password to proceed');
        });
    }
    
    /**
        * Test Case ID - 13
        *
        * Title  - Director's login page authentication system rejects unexisting users.
        *
        * Purpose - This test case ensures that authentication system on director login page performs correctly
        *           by rejecting users that are not in the database.
        * This is accomplished by first visiting /director/login and trying to submit form with incorrect
        * data. After that we assert that we see error message.
        *
        * @return void
    */
    public function test_director_login_page_authentication_system_rejects_unexisting_users() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/director/login')
                    ->type('username', 'test_user')
                    ->type('password', 'test123')
                    ->press('Login')
                    // error message is displayed
                    ->assertSee('These credentials')
                    ->assertPathIs('/director/login');
        });
    }
    
    /**
        * Test Case ID - 14
        *
        * Title  - Director's login page authentication logs in existing users.
        *
        * Purpose - This test case ensures that authentication system on director login page performs correctly
        *           by accepting existing users.
        * This is accomplished by first visiting /director/login and trying to submit form with correct
        * data. After that we assert that we are being redirected to the dashboard and that it contains
        * welcome message.
        *
        * @return void
    */
    public function test_director_login_page_authenticates_existing_users() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/director/login')
                    ->type('username', 'd12345')
                    ->type('password', 'filip123')
                    ->press('Login')
                    ->assertPathIs('/director/dashboard')
                    ->assertSee('Welcome,');
        });
    }
}
