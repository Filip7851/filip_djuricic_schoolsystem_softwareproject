<?php

namespace Tests\Browser\DirectorTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\Manager;
use App\Department;

class DirectorBrowsingManagersTest extends DuskTestCase
{
    /**
        * Test Case ID - 20
        *
        * Title  - Director browsing managers route is accessible
        *
        * Purpose - This test case asserts that users can access director login page.
        * This is simply accomplished by accessing /director/browse_managers and asserting path & that we see appropriate text.
        *
        * @return void
    */  
    public function test_director_browsing_managers_route_is_accessible()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/browse_managers')
                    ->assertPathIs('/director/browse_managers')
                    ->assertSee('Browse Managers');
        });
    }
    
    /**
        * Test Case ID - 21
        *
        * Title  - Director browsing managers performs correctly
        *
        * Purpose - This test case asserts that browsing manager functionality performs correctly.
        * This is simply accomplished by accessing /director/browse_managers that we see names, emails, last names, etc. of managers.
        *
        * @return void
    */ 
    public function test_director_browsing_managers_performs_correctly() 
    {
        
        $this->browse(function (Browser $browser) {
            $managers = Manager::all();
            
            foreach ($managers as $manager) {
                $browser->loginAs(Director::find(1), 'director')
                        ->visit('/director/browse_managers')
                        ->assertSeeIn('#datatable', $manager->first_name)
                        ->assertSeeIn('#datatable', $manager->last_name)
                        ->assertSeeIn('#datatable', $manager->email)
                        ->assertSeeIn('#datatable', $manager->id_card)
                        ->assertSeeIn('#datatable', $manager->department->department_name);
            }
        });
    }
}
