<?php

namespace Tests\Browser\DirectorTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\Manager;
use App\Department;

class DirectorAddingManagerTest extends DuskTestCase
{
    /**
        * Test Case ID - 15
        *
        * Title  - Director adding manager route is accessible
        *
        * Purpose - This test case asserts that users can access director login page.
        * This is simply accomplished by accessing /director/add_manager and asserting path & that we see appropriate text.
        *
        * @return void
    */    
    public function test_director_adding_manager_route_is_accessible() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/add_manager')
                    ->assertSee('Add New Manager');
        });
    }
    
    /**
        * Test Case ID - 16
        *
        * Title  - Director adding manager is not available when departments are full
        *
        * Purpose - This test case asserts that managers can no longer be added once the all departments
        * are filled with managers.
        *
        * @return void
    */  
    public function test_director_adding_manager_not_available_when_departments_are_full() 
    {
        $this->browse(function (Browser $browser) {
            $departmentsHavingManagers = Department::where('contained', 1)->count();
            
            // all depts have manager
            if($departmentsHavingManagers == 4)
            {
                $browser->loginAs(Director::find(1), 'director')
                        ->visit('/director/add_manager')
                        ->assertSee('All departments currently have manager');
            }
            else
            {
                $browser->loginAs(Director::find(1), 'director')
                        ->visit('/director/add_manager')
                        ->assertSee('First Name')
                        ->assertSee('Last Name');
            }
        });
    }
    
    /**
        * Test Case ID - 17
        *
        * Title  - Director adding manager form validation performs correctly.
        *
        * Purpose - This test case asserts that form validation for adding manager performs correctly.
        * It is accomplished by opening page and inserting all empty values into text boxes and asserting that all error messages are shown.
        *
        * @return void
    */  
    public function test_director_adding_manager_form_validation_performs_correctly()
    {
        $this->browse(function (Browser $browser) {
            // drop networks manager
            $manager = Manager::where('department_id', 3)->get()->first();
            $dept = Department::where('department_id', $manager->department_id)->get()->first();
            $dept->contained = 0;
            $dept->save();
            $manager = Manager::destroy($manager->id);
            
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/add_manager')
                    ->type('first_name', '')
                    ->type('last_name', '')
                    ->type('email', '')
                    ->type('id_card', '')
                    ->press('Add')
                    ->assertSee('Please Enter Manager\'s First Name')
                    ->assertSee('Please Enter Manager\'s Last Name')
                    ->assertSee('Please Enter Manager\'s Email')
                    ->assertSee('Please Enter ID Card');
            
            // re-insert networks manager
            $newManager = Manager::create([
                'department_id' => 3,
                'first_name' => 'Networks',
                'last_name' => 'Manager',
                'email' => 'networks@school.com',
                'id_card' => '0133124C',
                'username' => 'networksm',
                'password' => 'school'.mb_substr('0133124C', 0, 7),
                'in_convo' => 0
            ]);
            
            $dept->contained = 1;
            $dept->save();
        });
    }
    
    /**
        * Test Case ID - 18
        *
        * Title  - Director adding manager form displays only departments without managers.
        *
        * Purpose - This test case asserts that adding manager form displays only departments without manager.
        * It is accomplished by first modifying records in the DB to ensure that contained (flag that determines whether there is manager)
        * is 1 (meaning there is). After that we assert that we see those elements in select box of departments.
        *
        * @return void
    */  
    public function test_director_adding_manager_only_departments_without_manager_are_shown()
    {
        $this->browse(function (Browser $browser) {
            $firstDept = Department::find(1);
            $secondDept = Department::find(4);
            $firstGoBackToOne = false;
            $secondGoBackToOne = false;
            
            if ($firstDept->contained == 1)
            {
                $firstDept->contained = 0;
                $firstGoBackToOne = true;
            }
            if($secondDept->contained == 1)
            {
                $secondDept->contained = 0;
                $secondGoBackToOne = true;   
            }
            
            $firstDept->save();
            $secondDept->save();
            
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/add_manager')
                    ->assertSeeIn('#depts', 'Software Development')
                    ->assertSeeIn('#depts', 'End-User Support');
            
            if($firstGoBackToOne)
            {
                $firstDept->contained = 1;
                $firstDept->save();
            }
            
            if($secondGoBackToOne)
            {
                $secondDept->contained = 1;
                $secondDept->save();   
            }
        });
    }
    
    /**
        * Test Case ID - 19
        *
        * Title  - Director adding manager functionality performs correctly.
        *
        * Purpose - This test case asserts that adding manager functionality is performing correctly.
        * It is accomplished by accessing /director/add_manager and inserting correct data and asserting that later when browsing manager
        * we can see added manager into the table.
        *
        * @return void
    */  
    public function test_director_adding_manager_functionality_performs_correctly()
    {
        $this->browse(function (Browser $browser) {
            // drop networks manager
            $manager = Manager::where('department_id', 3)->get()->first();
            $dept = Department::where('department_id', $manager->department_id)->get()->first();
            $dept->contained = 0;
            $dept->save();
            $manager = Manager::destroy($manager->id);
            
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/add_manager')
                    ->type('first_name', 'Test')
                    ->type('last_name', 'Networks')
                    ->type('email', 'networks@school.com')
                    ->type('id_card', '0133124C')
                    ->press('Add')
                    ->assertPathIs('/director/browse_managers')
                    ->assertSeeIn('#datatable', 'Test')
                    ->assertSeeIn('#datatable', 'Networks')
                    ->assertSeeIn('#datatable', 'networks@school.com')
                    ->assertSeeIn('#datatable', '0133124C')
                    ->assertSeeIn('#datatable', 'Computer Networks');
        });
    }
}
