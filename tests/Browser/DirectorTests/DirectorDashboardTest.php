<?php

namespace Tests\Browser\DirectorTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\Manager;
use App\Lecturer;
use App\Student;

class DirectorDashboardTest extends DuskTestCase
{
    /**
        * Test Case ID - 22
        *
        * Title  - Director dashboard displays total amount of users
        *
        * Purpose - This test case asserts that director dashboard displays total number of users within system.
        * This is simply accomplished by retrieving total number of students, lecturers, managers, direcetors and summing them.
        * After that, we assert that we see it in element with id of #tot_cou.
        *
        * @return void
    */ 
    public function test_director_total_amount_of_users_is_displayed()
    {
        $this->browse(function (Browser $browser) {
            $totalStudents = Student::all()->count();
            $totalLecturers = Lecturer::all()->count();
            $totalManagers = Manager::all()->count();
            $totalDirectors = Director::all()->count();
            $totalUsers = $totalStudents + $totalLecturers + $totalManagers + $totalDirectors;
            
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSeeIn('#tot_cou', $totalUsers);
        });
    }
    
    /**
        * Test Case ID - 23
        *
        * Title  - Director dashboard displays total number of students
        *
        * Purpose - This test case asserts that director dashboard displays total number of students within system.
        * This is simply accomplished by retrieving total number of students.
        * After that, we assert that we see it in element with id of #tot_stud.
        *
        * @return void
    */ 
    public function test_director_total_amount_of_students_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $totalStudents = Student::all()->count();
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSeeIn('#tot_stud', $totalStudents);
        });
    }
    
    /**
        * Test Case ID - 24
        *
        * Title  - Director dashboard displays total number of lecturers
        *
        * Purpose - This test case asserts that director dashboard displays total number of lecturers within system.
        * This is simply accomplished by retrieving total number of lecturers.
        * After that, we assert that we see it in element with id of #tot_lec.
        *
        * @return void
    */ 
    public function test_director_total_amount_of_lecturers_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $totalLecturers = Lecturer::all()->count();
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSeeIn('#tot_lec', $totalLecturers);
        });
    }
    
    /**
        * Test Case ID - 25
        *
        * Title  - Director dashboard displays total number of managers
        *
        * Purpose - This test case asserts that director dashboard displays total number of managers within system.
        * This is simply accomplished by retrieving total number of managers.
        * After that, we assert that we see it in element with id of #tot_man.
        *
        * @return void
    */ 
    public function test_director_total_amount_of_managers_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $totalManagers = Manager::all()->count();
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSeeIn('#tot_man', $totalManagers);
        });
    }
    
    /**
        * Test Case ID - 26
        *
        * Title  - Director dashboard displays total number of female students
        *
        * Purpose - This test case asserts that director dashboard displays total number of female students within system.
        * This is simply accomplished by retrieving total number of female students.
        * After that, we assert that we see it in element with id of #tot_f_stud.
        *
        * @return void
    */ 
    public function test_director_total_amount_of_female_students_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $totalFemaleStudents = Student::where('gender', 1)->count();
            
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSeeIn('#tot_f_stud', $totalFemaleStudents);
        });
    }
    
    /**
        * Test Case ID - 27
        *
        * Title  - Director dashboard displays total number of male students
        *
        * Purpose - This test case asserts that director dashboard displays total number of male students within system.
        * This is simply accomplished by retrieving total number of male students.
        * After that, we assert that we see it in element with id of #tot_m_stud.
        *
        * @return void
    */ 
    public function test_director_total_amount_of_male_students_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $totalMaleStudents = Student::where('gender', 0)->count();
            
            $browser->loginAs(Director::find(1), 'director')
                    ->visit('/director/dashboard')
                    ->assertSeeIn('#tot_m_stud', $totalMaleStudents);
        });
    }
}
