<?php

namespace Tests\Browser\ManagerTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\Manager;
use App\Lecturer;
use App\Student;

class ManagerDashboardTest extends DuskTestCase
{
    /**
     * Test Case ID - 58
     *
     * Title  - Manager dashboard displays total amount of users within department
     *
     * Purpose - This test case asserts that manager dashboard displays total number of users within department.
     * This is simply accomplished by retrieving total number of students, lecturers and managers within department and summing them.
     * After that, we assert that we see it in element with id of #tot_use_dept.
     *
     * @return void
     */
    public function test_manager_total_amount_of_users_in_department_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $manager = Manager::find(1);
            $totalLecturersInDept = Lecturer::where('department_id', $manager->department_id)->count();
            $totalStudentsInDept = Student::where('department_id', $manager->department_id)->count();
            
            $totalUsersInDept = $totalLecturersInDept + $totalStudentsInDept + 1;
            
            $browser->loginAs($manager, 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->assertSeeIn('#tot_use_dept', $totalUsersInDept);
        });
    }

    /**
     * Test Case ID - 59
     *
     * Title  - Manager dashboard displays total amount of lecturers within department
     *
     * Purpose - This test case asserts that manager dashboard displays total number of lecturers within department.
     * This is simply accomplished by retrieving total number of lecturers within department and 
     * asserting that we see it in element with id of #tot_lec_dept.
     * 
     * @return void
     */
    public function test_manager_total_amount_of_lecturers_in_department_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $manager = Manager::find(1);
            $totalLecturersInDept = Lecturer::where('department_id', $manager->department_id)->count();
            
            $browser->loginAs($manager, 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->assertSeeIn('#tot_lec_dept', $totalLecturersInDept);
        });
    }

    /**
     * Test Case ID - 60
     *
     * Title  - Manager dashboard displays total amount of students within department
     *
     * Purpose - This test case asserts that manager dashboard displays total number of students within department.
     * This is simply accomplished by retrieving total number of students within department and
     * asserting that we see it in element with id of #tot_stu_dept.
     *
     * @return void
     */
    public function test_manager_total_amount_of_students_in_department_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $manager = Manager::find(1);
            $totalStudentsInDept = Student::where('department_id', $manager->department_id)->count();
            
            $browser->loginAs($manager, 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->assertSeeIn('#tot_stu_dept', $totalStudentsInDept);
        });
    }

    /**
     * Test Case ID - 61
     *
     * Title  - Manager dashboard displays total amount of male students within department
     *
     * Purpose - This test case asserts that manager dashboard displays total number of male students within department.
     * This is simply accomplished by retrieving total number of male students within department and
     * asserting that we see it in element with id of #tot_m_stu_dept.
     *
     * @return void
     */
    public function test_manager_total_amount_of_male_students_in_department_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $manager = Manager::find(1);
            $totalMaleStudentsInDept = Student::where('department_id', $manager->department_id)->where('gender', 0)->count();
            
            $browser->loginAs($manager, 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->assertSeeIn('#tot_m_stu_dept', $totalMaleStudentsInDept);
        });
    }

    /**
     * Test Case ID - 62
     *
     * Title  - Manager dashboard displays total amount of female students within department
     *
     * Purpose - This test case asserts that manager dashboard displays total number of female students within department.
     * This is simply accomplished by retrieving total number of female students within department and
     * asserting that we see it in element with id of #tot_f_stu_dept.
     *
     * @return void
     */
    public function test_manager_total_amount_of_female_students_in_department_is_displayed() 
    {
        $this->browse(function (Browser $browser) {
            $manager = Manager::find(1);
            $totalFemaleStudentsInDept = Student::where('department_id', $manager->department_id)->where('gender', 1)->count();            
            
            $browser->loginAs($manager, 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->assertSeeIn('#tot_f_stu_dept', $totalFemaleStudentsInDept);
        });
    }
}
