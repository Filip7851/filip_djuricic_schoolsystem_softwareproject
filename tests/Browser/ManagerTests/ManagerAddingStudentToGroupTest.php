<?php

namespace Tests\Browser\ManagerTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Manager;
use App\Group;
use App\Student;

class ManagerAddingStudentToGroupTest extends DuskTestCase
{
    /**
     * Test Case ID - 53
     *
     * Title  - Manager adding student form is innacessible if no classes are registered.
     *
     * Purpose - This test case asserts that adding student form is unavailable when no classes are registered.
     * It is accomplished by accessing first dropping all classes from the database, accessing /manager/add_student and then
     * asserting that we see 'No Classes' message which indicates that form is not available.
     * After that we revert all groups and re-insert them.
     *
     * @return void
     */
    public function test_manager_adding_student_to_class_innacessible_if_no_classes() 
    {
        $this->browse(function (Browser $browser) {
            $initialGroups = Group::all();
            $groups = $initialGroups;
            
            foreach($initialGroups as $initialGroup)
            {
                Group::destroy($initialGroup->pluck('group_id'));
            }
            
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_student')
                    ->assertSee('No Classes');
            
            $pk = 1;
            
            foreach($groups as $key => $group)
            {
                Group::create([
                    'group_id' => $pk,
                    'department_id' => $group->department_id,
                    'group_name' => $group->group_name,
                    'level' => $group->level
                ]);
                $pk++;
            }
        });
    }
    
    /**
     * Test Case ID - 54
     *
     * Title  - Manager adding student form is accessible if classes are registered.
     *
     * Purpose - This test case asserts that adding student form is available when classes are registered.
     * It is accomplished by accessing /manager/add_student and asserting that we see 'Add New Student' which indicates that form exists.
     *
     * @return void
     */
    public function test_manager_adding_student_to_class_innacessible_if_classes_exist() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_student')
                    ->assertPathIs('/manager/add_student')
                    ->assertSee('Add New Student');
        });
    }

    /**
     * Test Case ID - 55
     *
     * Title  - Manager adding student form validation performs correctly.
     *
     * Purpose - This test case asserts that form validation for adding student performs correctly.
     * It is accomplished by opening page and inserting all empty values into text boxes and asserting that all error messages are shown.
     *
     * @return void
     */
    public function test_manager_adding_student_form_validation_performs_correctly() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_student')
                    ->assertPathIs('/manager/add_student')
                    ->type('first_name', '')
                    ->type('last_name', '')
                    ->type('email', '')
                    ->type('id_card', '')
                    ->press('Add')
                    ->assertPathIs('/manager/add_student')
                    ->assertSee('Please enter first name to proceed')
                    ->assertSee('Please enter last name to proceed')
                    ->assertSee('Please enter email to proceed')
                    ->assertSee('Please enter ID Card to proceed')
                    ->assertSee('Please choose student gender to proceed');
        });
    }

    /**
     * Test Case ID - 56
     *
     * Title  - Manager adding student functionality performs correctly (level 4 students).
     *
     * Purpose - This test case asserts that adding student functionality for level 4 students is performing correctly.
     * It is accomplished by accessing /manager/add_group and inserting correct data and asserting that later when browsing classes
     * we can see added student in the table.
     * 
     * After that, we remove the testing student record from the database.
     *
     * @return void
     */
    public function test_manager_adding_student_performs_correctly_lvl_4() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_student')
                    ->assertPathIs('/manager/add_student')
                    ->type('first_name', 'Test')
                    ->type('last_name', 'Dummy')
                    ->press('Male')
                    ->type('email', 'dummy@student.com')
                    ->type('id_card', '0112222Z')
                    ->select('group', Group::find(2)->group_id)
                    ->press('Add')
                    ->assertPathIs('/manager/browse_classes')
                    ->select('levels', Group::where('group_id', Student::all()->last()->group_id)->first()->level)
                    ->select('groups', Group::where('group_id', Student::all()->last()->group_id)->first()->group_id)
                    ->pause(2000)
                    ->assertSeeIn('#present', Student::all()->last()->first_name)
                    ->assertSeeIn('#present', Student::all()->last()->last_name)
                    ->assertSeeIn('#present', Student::all()->last()->email)
                    ->assertSeeIn('#present', Student::all()->last()->username);
        });
        
        Student::destroy(Student::all()->last()->student_id);
    }

    /**
     * Test Case ID - 57
     *
     * Title  - Manager adding student functionality performs correctly (level 6 students).
     *
     * Purpose - This test case asserts that adding student functionality for level 6 students is performing correctly.
     * It is accomplished by accessing /manager/add_group and inserting correct data and asserting that later when browsing classes
     * we can see added student in the table.
     *
     * After that, we remove the testing student record from the database.
     *
     * @return void
     */
    public function test_manager_adding_student_performs_correctly_lvl_6() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_student')
                    ->assertPathIs('/manager/add_student')
                    ->type('first_name', 'Test')
                    ->type('last_name', 'Dummy')
                    ->press('Male')
                    ->type('email', 'dummy@student.com')
                    ->type('id_card', '0112222Z')
                    ->select('group', Group::find(4)->group_id)
                    ->press('Add')
                    ->assertPathIs('/manager/browse_classes')
                    ->select('levels', Group::where('group_id', Student::all()->last()->group_id)->first()->level)
                    ->select('groups', Group::where('group_id', Student::all()->last()->group_id)->first()->group_id)
                    ->pause(2000)
                    ->assertSeeIn('#present', Student::all()->last()->first_name)
                    ->assertSeeIn('#present', Student::all()->last()->last_name)
                    ->assertSeeIn('#present', Student::all()->last()->email)
                    ->assertSeeIn('#present', Student::all()->last()->username);
        });
        
        Student::destroy(Student::all()->last()->student_id);
    }
}
