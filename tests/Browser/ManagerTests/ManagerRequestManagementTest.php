<?php

namespace Tests\Browser\ManagerTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Director;
use App\Manager;
use App\Demand;


class ManagerRequestManagementTest extends DuskTestCase
{
    /**
     * Test Case ID - 63
     *
     * Title  - Manager can't access request route if is innaccessible (solved already) or if it doesn't exist.
     *
     * Purpose - This test case asserts that manager can't access the request if it is already solved (either approved or rejected)
     * or if it doesn't exist.
     * This is simply accomplished by trying to access unexisting request and asserting that manager is redirected back.
     *
     * @return void
     */
    public function test_manager_make_request_route_is_innaccessible_if_unexisting_request() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->visit('/manager/read_more/unexisting_request')
                    // user returned back
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 64
     *
     * Title  - Manager can't access request if it is from other manager
     *
     * Purpose - This test case asserts that manager can't access the request if it is made by other manager.
     * This is simply accomplished by trying to access request which is made by other manager and asserting that 
     * we are redirected back.
     *
     * @return void
     */
    public function test_manager_cant_access_requests_from_other_managers() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->visit('/manager/read_more/'.Demand::first()->demand_id)
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 65
     *
     * Title  - Manager's make request form validation performs correctly.
     *
     * Purpose - This test case asserts that manager's make request form validation performs correctly.
     * This is simply accomplished accessing /manager/make_request form and inserting empty values into fields
     * and asserting that we see error messages.
     *
     * @return void
     */
    public function test_manager_make_request_form_validation_performs_correctly() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/make_request')
                    ->type('request_title', '')
                    ->type('request_body', '')
                    ->press('Make Request')
                    ->assertPathIs('/manager/make_request')
                    ->assertSee('Please enter title of the request to proceed')
                    ->assertSee('Please enter body of the request to proceed');
        });
    }

    /**
     * Test Case ID - 66
     *
     * Title  - Manager can make non-urgent requests
     *
     * Purpose - This test case asserts that manager can make non-urgent request to the director.
     * This is simply accomplished accessing /manager/make_request form and inserting correct values into fields
     * and NOT ticking urgent request.
     * After that, we login as director and assert that we see that request and that it is non-urgent.
     *
     * @return void
     */
    public function test_manager_make_non_urgent_request_performs_correctly() 
    {
        $this->browse(function ($managerBrowser, $directorBrowser) {
            $managerBrowser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/make_request')
                    ->type('request_title', 'Test Request')
                    ->type('request_body', 'This request is testing request. Will be deleted after test.')
                    ->press('Make Request')
                    ->assertPathIs('/manager/browse_requests')
                    ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                    ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                    ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name);
            $directorBrowser->loginAs(Director::find(1), 'director')
                            ->visit('/director/browse_requests')
                            ->assertPathIs('/director/browse_requests')
                            ->assertSeeIn('#demand_table', Manager::find(Demand::all()->last()->manager_id)->first_name.' '.Manager::find(Demand::all()->last()->manager_id)->last_name)
                            ->assertSeeIn('#demand_table', Demand::all()->last()->request_title);
            // dispose last demand
            Demand::destroy(Demand::all()->last()->demand_id);
        });
    }

    /**
     * Test Case ID - 67
     *
     * Title  - Manager can make urgent requests
     *
     * Purpose - This test case asserts that manager can make urgent request to the director.
     * This is simply accomplished accessing /manager/make_request form and inserting correct values into fields
     * and TICKING urgent request.
     * After that, we login as director and assert that we see that request and that it is urgent.
     *
     * @return void
     */
    public function test_manager_make_urgent_request_performs_correctly() 
    {
        $this->browse(function ($managerBrowser, $directorBrowser) {
            $managerBrowser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/make_request')
                    ->type('request_title', 'Test Request')
                    ->type('request_body', 'This request is testing request. Will be deleted after test.')
                    ->check('is_urgent')
                    ->press('Make Request')
                    ->assertPathIs('/manager/browse_requests')
                    ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                    ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                    ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name);
            $directorBrowser->loginAs(Director::find(1), 'director')
                   ->visit('/director/browse_requests')
                   ->assertPathIs('/director/browse_requests')
                   ->assertSeeIn('#demand_table', Manager::find(Demand::all()->last()->manager_id)->first_name.' '.Manager::find(Demand::all()->last()->manager_id)->last_name)
                   ->assertSeeIn('#demand_table', Demand::all()->last()->request_title);
            // dispose last demand
            Demand::destroy(Demand::all()->last()->demand_id);
        });
    }

    /**
     * Test Case ID - 68
     *
     * Title  - Manager can make non-urgent requests
     *
     * Purpose - This test case asserts that manager can read more about request.
     * This is simply accomplished accessing /manager/make_request, making testing request, and accessing /manager/read_more page
     * and asserting that data is appropriately displayed.
     * After test, we dispose testing request.
     *
     * @return void
     */
    public function test_manager_can_read_more_about_his_request() 
    {
        $this->browse(function (Browser $browser) {
            $manager = Manager::find(1);
            // save copy of all demands to be re-created.
            $oldDemands = $manager->demands;
            
            foreach($manager->demands as $demand)
            {
                $demand->delete();    
            }
            
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->visit('/manager/make_request')
                    ->type('request_title', 'Test Request')
                    ->type('request_body', 'This request is testing request. Will be deleted after test.')
                    ->check('is_urgent')
                    ->press('Make Request')
                    ->assertPathIs('/manager/browse_requests')
                    ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                    ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                    ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name)
                    ->clickLink('Open')
                    ->assertPathIs('/manager/read_more/'.Demand::all()->last()->demand_id)
                    ->assertSeeIn('.inbox-body', 'Status: UNRESOLVED')
                    ->assertSeeIn('.inbox-body', Demand::all()->last()->request_title)
                    ->assertSeeIn('.inbox-body', Demand::all()->last()->request_body)
                    ->assertSeeIn('.inbox-body', Manager::find(1)->email);        
             // dispose last demand
             Demand::destroy(Demand::all()->last()->demand_id);
             
             foreach($oldDemands as $d)
             {
                 Demand::create([
                     'demand_id' => $d->demand_id,
                     'manager_id' => $d->manager_id,
                     'director_id' => $d->director_id,
                     'is_urgent' => $d->is_urgent,
                     'request_title' => $d->request_title,
                     'request_body' => $d->request_body,
                     'approved' => $d->approved,
                     'student_id' => null,
                     'lecturer_id' => null
                 ]);
             }
        });
    }

    /**
     * Test Case ID - 69
     *
     * Title  - Manager's resolved requests are displayed correctly and can be removed
     *
     * Purpose - This test case asserts that manager receives resolved request correctly and can remove it.
     * This is simply accomplished accessing /manager/make_request, making 2 test requests, after that
     * we login as director, Approve one of those requests and reject other request and then login again as manager.
     * After that we once again login as manager and assert that we see that request is resolved and that 
     * it can be removed.
     *
     * @return void
     */
    public function test_manager_when_request_is_resolved_it_is_displayed_correctly_and_manager_can_remove_it() 
    {
        $this->browse(function ($managerBrowser, $directorBrowser, $secondManagerBrowser) {
             $manager = Manager::find(1);
             // save copy of all demands to be re-created.
             $oldDemands = $manager->demands;
 
             foreach($manager->demands as $demand)
             {
                 $demand->delete();
             }
             
             $managerBrowser->loginAs(Manager::find(1), 'manager')
                            ->visit('/manager/make_request')
                            ->type('request_title', 'Custom Request')
                            ->type('request_body', 'This request is testing request. Will be deleted after test.')
                            ->press('Make Request')
                            ->assertPathIs('/manager/browse_requests')
                            ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                            ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                            ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name)
                            ->visit('/manager/make_request')
                            ->type('request_title', 'Test Request 2')
                            ->type('request_body', 'This request is second testing request. Will also be deleted after test.')
                            ->press('Make Request')
                            ->assertPathIs('/manager/browse_requests')
                            ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                            ->assertSeeIn('#demand_table', Demand::all()->last()->request_title)
                            ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name);
            
            $lastDemand = Demand::all()->last();
            $prevDemand = Demand::find(($lastDemand->demand_id-1));     
            
            
            $directorBrowser->loginAs(Director::find(1), 'director')
                            ->visit('/director/browse_requests')
                            ->assertPathIs('/director/browse_requests')
                            ->assertSeeIn('#demand_table', Manager::find($prevDemand->manager_id)->first_name.' '.Manager::find($prevDemand->manager_id)->last_name)
                            ->assertSeeIn('#demand_table', $prevDemand->request_title)
                            ->visit(route('director.requests.read_more', ['request_id' => $prevDemand->demand_id]))
                            ->assertPathIs('/director/request_more/'.$prevDemand->demand_id)
                            ->assertSeeIn('.inbox-body', $prevDemand->request_title)
                            ->assertSeeIn('.inbox-body', $prevDemand->request_body)
                            ->assertSeeIn('.inbox-body', Manager::find($prevDemand->manager_id)->email)
                            ->press('Approve')
                            ->assertPathIs('/director/browse_requests')
                            ->assertDontSeeIn('#demand_table', $prevDemand->request_title)
                            // / last_demand - 1
                            // last_demand
                            ->visit(route('director.requests.read_more', ['request_id' => $lastDemand->demand_id]))
                            ->assertPathIs('/director/request_more/'.$lastDemand->demand_id)
                            ->assertSeeIn('.inbox-body', $lastDemand->request_title)
                            ->assertSeeIn('.inbox-body', $lastDemand->request_body)
                            ->assertSeeIn('.inbox-body', Manager::find($lastDemand->manager_id)->email)
                            ->press('Reject')
                            ->assertPathIs('/director/browse_requests');
            
            $secondManagerBrowser->loginAs(Manager::find(1), 'manager')
                           ->visit('/manager/browse_requests')
                           ->assertSeeIn('#demand_table', Manager::find(1)->first_name.' '.Manager::find(1)->last_name)
                           ->assertSeeIn('#demand_table', $prevDemand->request_title)
                           ->assertSeeIn('#demand_table', Manager::find(1)->department->department_name)
                           ->assertSeeIn('#demand_table', 'ACCEPTED')
                           ->visit(route('manager.requests.read_more', ['request_id' => $prevDemand->demand_id]))
                           ->assertPathIs('/manager/read_more/'.$prevDemand->demand_id)
                           ->assertSeeIn('.inbox-body', $prevDemand->request_title)
                           ->assertSeeIn('.inbox-body', $prevDemand->request_body)
                           ->assertSeeIn('.inbox-body', Manager::find(1)->email)
                           ->press('Remove')
                           ->assertPathIs('/manager/browse_requests')
                           ->assertDontSeeIn('#demand_table', $prevDemand->request_body)
                           ->visit(route('manager.requests.read_more', ['request_id' => $lastDemand->demand_id]))
                           ->assertPathIs('/manager/read_more/'.$lastDemand->demand_id)
                           ->assertSeeIn('.inbox-body', $lastDemand->request_title)
                           ->assertSeeIn('.inbox-body', $lastDemand->request_body)
                           ->assertSeeIn('.inbox-body', Manager::find(1)->email)
                           ->press('Remove')
                           ->assertPathIs('/manager/browse_requests')
                           // this manager has no more demands
                           ->assertSee('Currently, you have no requests, that are either approved, rejected or pending');
            foreach($oldDemands as $d)
            {
                Demand::create([
                    'demand_id' => $d->demand_id,
                    'manager_id' => $d->manager_id,
                    'director_id' => $d->director_id,
                    'is_urgent' => $d->is_urgent,
                    'request_title' => $d->request_title,
                    'request_body' => $d->request_body,
                    'approved' => $d->approved,
                    'student_id' => null,
                    'lecturer_id' => null
                ]);
            }
        });
    }
}
