<?php

namespace Tests\Browser\ManagerTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Manager;

class ManagerEditingProfileTest extends DuskTestCase
{
    /**
     * Test Case ID - 70
     * 
     * Title - Manager can modify his profile
     * 
     * Purpose - This test case asserts that manager can modify his profile.
     * This is simply done by first accessing /manager/edit_profile and asserting that we see current data in the text boxes
     * After that we insert custom modify data and we then visit again /manager/edit_profile to assert that new values are inserted.
     * After the test, we revert manager data to pre-edited data.
     *
     * @return void
     */
    public function test_manager_can_edit_his_profile()
    {
        $this->browse(function (Browser $browser) {
            $manager = Manager::find(1);
            $oldManager = $manager;
            
            $browser->loginAs($manager, 'manager')
                    ->visit('/manager/edit_profile')
                    ->assertPathIs('/manager/edit_profile')
                    ->assertValue('#first_name', $manager->first_name)
                    ->assertValue('#last_name', $manager->last_name)
                    ->assertValue('#email', $manager->email)
                    ->assertValue('#id_card', $manager->id_card)
                    ->type('first_name', $manager->first_name.' EDIT')
                    ->type('last_name', $manager->last_name.' EDIT')
                    ->type('first_name', $manager->email.'EDIT')
                    ->type('id_card', $manager->id_card.' EDIT')
                    ->visit('/manager/edit_profile')
                    ->assertValue('#first_name', $manager->first_name)
                    ->assertValue('#last_name', $manager->last_name)
                    ->assertValue('#email', $manager->email)
                    ->assertValue('#id_card', $manager->id_card);
            
            $manager->first_name = $oldManager->first_name;
            $manager->last_name = $oldManager->last_name;
            $manager->email = $oldManager->email;
            $manager->id_card = $oldManager->id_card;
            $manager->save();
        });
    }
}