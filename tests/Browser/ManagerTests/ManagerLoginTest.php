<?php

namespace Tests\Browser\ManagerTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ManagerLoginTest extends DuskTestCase
{
    /**
     * Test Case ID - 44
     *
     * Title  - Manager dashboard can't be accessed before login.
     *
     * Purpose - This test case ensures that guest (non-authenticated) users can't access manager
     *           dashboard before logging in.
     * This is accomplished by trying to visit /manager/dashboard without authenticating.
     * We assert that we are redirected back to the original route /manager/login.
     *
     * @return void
     */
    public function test_manager_dashboard_cant_be_accessed_before_login() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/manager/login')
                    ->assertSee('Manager Login')
                    ->visit('/manager/dashboard')
                    ->assertPathIs('/manager/login');
        });
    }

    /**
     * Test Case ID - 45
     *
     * Title  - Manager login page is accessible
     *
     * Purpose - This test case asserts that users can access manager login page.
     * This is simply accomplished by accessing /manager/login without logging in.
     *
     * @return void
     */
    public function test_manager_login_page_is_accessible() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/manager/login')
                    ->assertSee('Manager Login');
        });
    }

    /**
     * Test Case ID - 46
     *
     * Title  - Manager's login page form validation performs correctly
     *
     * Purpose - This test case ensures that form validation on manager login page performs correctly.
     * This is accomplished by first visiting /manager/login and trying to submit form without
     * inputing any value in the text field and asserting that we see error messages.
     *
     * @return void
     */
    public function test_manager_login_page_form_validation_performs_correctly()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/manager/login')
                    ->type('username', '')
                    ->type('password', '')
                    ->press('Login')
                    ->assertSee('Please enter your username to proceed')
                    ->assertSee('Please enter your password to proceed');
        });
    }

    /**
     * Test Case ID - 47
     *
     * Title  - Manager's login page form validation works on single fields.
     *
     * Purpose - This test case ensures that form validation on manager login page performs correctly
     *           if only single fields contain error such as missing input.
     * This is accomplished by first visiting /manager/login and trying to submit form with one field
     * such as username filled and one field such as password empty and vice-versa.
     * After that we assert that we see error messages.
     *
     * @return void
     */
    public function test_manager_login_page_form_validation_single_fields() 
    {
        $this->browse(function ($first, $second) {
            $first->visit('/manager/login')
                    ->type('username', '')
                    ->type('password', 'test')
                    ->press('Login')
                    ->assertSee('Please enter your username to proceed')
                    ->assertDontSee('Please enter your password to proceed');
            $second->visit('/manager/login')
                   ->type('username', 'director')
                   ->type('password', '')
                   ->press('Login')
                   ->assertDontSee('Please enter your username to proceed')
                   ->assertSee('Please enter your password to proceed');
        });
    }

    /**
     * Test Case ID - 48
     *
     * Title  - Manager's login page authentication system rejects unexisting users.
     *
     * Purpose - This test case ensures that authentication system on manager login page performs correctly
     *           by rejecting users that are not in the database.
     * This is accomplished by first visiting /manager/login and trying to submit form with incorrect
     * data. After that we assert that we see error message.
     *
     * @return void
     */
    public function test_manager_login_page_authentication_system_rejects_unexisting_users() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/manager/login')
                    ->type('username', 'test_user')
                    ->type('password', 'test123')
                    ->press('Login')
                    ->assertSee('These credentials');
        });
    }

    /**
     * Test Case ID - 49
     *
     * Title  - Manager's login page authentication logs in existing users.
     *
     * Purpose - This test case ensures that authentication system on manager login page performs correctly
     *           by accepting existing users.
     * This is accomplished by first visiting /manager/login and trying to submit form with correct
     * data. After that we assert that we are being redirected to the dashboard and that it contains
     * welcome message.
     *
     * @return void
     */
    public function test_manager_login_page_authenticates_existing_users() 
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/manager/login')
                    ->type('username', 'filipd')
                    ->type('password', 'school0124801')
                    ->press('Login')
                    ->assertPathIs('/manager/dashboard')
                    ->assertSee('Welcome,');
        });
    }
}
