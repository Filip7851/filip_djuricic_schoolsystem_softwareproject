<?php

namespace Tests\Browser\ManagerTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Manager;
use App\Group;

class ManagerAddingGroupTest extends DuskTestCase
{
    /**
     * Test Case ID - 50
     *
     * Title  - Manager adding group route is accessible
     *
     * Purpose - This test case asserts that managers can access add group page.
     * This is simply accomplished by first logging in as manager
     * and then accessing /manager/add_group and asserting path & that we see appropriate text.
     *
     * @return void
     */
    public function test_manager_adding_group_route_is_accessible() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_group')
                    ->assertPathIs('/manager/add_group')
                    ->assertSee('Add New Group');
        });
    }

    /**
     * Test Case ID - 51
     *
     * Title  - Manager adding group form validation performs correctly.
     *
     * Purpose - This test case asserts that form validation for adding group performs correctly.
     * It is accomplished by opening page and inserting all empty values into text boxes and asserting that all error messages are shown.
     *
     * @return void
     */
    public function test_manager_adding_group_form_validation_performs_correctly() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_group')
                    ->type('class_no', '')
                    ->press('Add')
                    ->assertSee('Please enter class number');
        });
    }

    /**
     * Test Case ID - 52
     *
     * Title  - Manager adding group functionality performs correctly.
     *
     * Purpose - This test case asserts that adding group functionality is performing correctly.
     * It is accomplished by accessing /manager/add_group and inserting correct data and asserting that later when browsing classes
     * we can see added class in the table.
     *
     * @return void
     */
    public function test_manager_adding_group_performs_correctly() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/add_group')
                    ->type('class_no', '1W')
                    ->press('Add')
                    ->assertPathIs('/manager/browse_classes')
                    ->assertSeeIn('#groups', Group::all()->last()->group_name);
        });
        
        Group::destroy(Group::all()->last()->group_id);
    }
}
