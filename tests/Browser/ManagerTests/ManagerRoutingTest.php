<?php

namespace Tests\Browser\ManagerTests;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Manager;

class ManagerRoutingTests extends DuskTestCase
{
    /**
     * Test Case ID - 36
     *
     * Title - Manager can't access login page after login
     *
     * Purpose - This test case ensures that manager auth guard cannot access the login page
     * after already logging in.
     * This is accomplished by first logging in manager and then trying to access the login page again
     * and asserting that manager is redirected back to the previous page.
     *
     * @return void
     */
    public function test_manager_cant_access_login_page_after_login()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                ->visit('/manager/dashboard')
                ->assertSee('Welcome,')
                ->visit('/manager/login')
                ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 37
     *
     * Title - Manager can't access director login page
     *
     * Purpose - This test case ensures that manager auth guard cannot access the director login page
     * after already logging in.
     * This is accomplished by first logging in manager and then trying to access the director login page
     * and asserting that manager is redirected back to the previous page.
     *
     * @return void
     */
    public function test_manager_cant_access_director_login_page() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/director/login')
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 38
     *
     * Title - Manager can't access lecturer login page
     *
     * Purpose - This test case ensures that manager auth guard cannot access the lecturer login page
     * after already logging in.
     * This is accomplished by first logging in manager and then trying to access the lecturer login page
     * and asserting that manager is redirected back to the previous page.
     *
     * @return void
     */
    public function test_manager_cant_access_lecturer_login_page() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/lecturer/login')
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 39
     *
     * Title - Manager can't access student login page
     *
     * Purpose - This test case ensures that manager auth guard cannot access the student login page
     * after already logging in.
     * This is accomplished by first logging in manager and then trying to access the student login page
     * and asserting that manager is redirected back to the previous page.
     *
     * @return void
     */
    public function test_manager_cant_access_student_login_page() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/student/login')
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 40
     *
     * Title - Manager can't access director routes
     *
     * Purpose - This test case ensures that manager auth guard cannot access the director-specific routes.
     * This is accomplished by first logging in manager and then trying to access the director-specific route
     * and asserting that manager is redirected back to the previous page.
     * Testing only on one route such as /director/dashboard is sufficient since same middleware is applied
     * to all routes within DirectorController so it essentially protects all routes.
     *
     * @return void
     */
    public function test_manager_cant_access_director_routes() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/director/dashboard')
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 41
     *
     * Title - Manager can't access lecturer routes
     *
     * Purpose - This test case ensures that manager auth guard cannot access the lecturer-specific routes.
     * This is accomplished by first logging in manager and then trying to access the lecturer-specific route
     * and asserting that manager is redirected back to the previous page.
     * Testing only on one route such as /lecturer/dashboard is sufficient since same middleware is applied
     * to all routes within LecturerController so it essentially protects all routes.
     *
     * @return void
     */
    public function test_manager_cant_access_lecturer_routes() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/lecturer/dashboard')
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 42
     *
     * Title - Manager can't access student routes
     *
     * Purpose - This test case ensures that manager auth guard cannot access the student-specific routes.
     * This is accomplished by first logging in manager and then trying to access the student-specific route
     * and asserting that manager is redirected back to the previous page.
     * Testing only on one route such as /student/dashboard is sufficient since same middleware is applied
     * to all routes within StudentController so it essentially protects all routes.
     *
     * @return void
     */
    public function test_manager_cant_access_student_routes() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/student/dashboard')
                    ->assertPathIs('/manager/dashboard');
        });
    }

    /**
     * Test Case ID - 43
     *
     * Title  - Manager receives 404 page if accessing unexisting route.
     *
     * Purpose - This test case ensures that manager auth guard receives 404 error informing him that
     *           he is trying to access non-existing route.
     *
     * This is accomplished by first logging in manager and then trying to access the non-existing route
     * and asserting that manager is shown 404 page.
     *
     * @return void
     */
    public function test_manager_404_page_shows() 
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(Manager::find(1), 'manager')
                    ->visit('/manager/dashboard')
                    ->assertSee('Welcome,')
                    ->visit('/unexisting_page/')
                    ->assertSee('404');
        });
    }
}
