<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TODO: Some kind of landing page.....
/*Route::get('/', function () {
    return view('admin.index');
})->name('admin.dashboard');
*/

Route::group(['prefix' => '/director'], function() {
    Route::get('/login', 'DirectorAuth\AuthenticatesDirectors@showLoginPage')->name('director.show');
    Route::post('/login', 'DirectorAuth\AuthenticatesDirectors@login')->name('director.login');
    Route::get('/dashboard', 'DirectorController@showDashboard')->name('director.dashboard');
    Route::get('/add_manager', 'DirectorController@showManagerForm')->name('director.manager.add');
    Route::post('/add_manager', 'DirectorController@addManager')->name('director.manager.insert');
    Route::get('/logout', 'DirectorAuth\AuthenticatesDirectors@logout')->name('director.logout');
    Route::get('/browse_managers', 'DirectorController@showManagerTable')->name('director.manager.browse');

    Route::get('/modify_manager/{id}', 'DirectorController@showModifyForm')->name('director.manager.show_edit');    
    Route::get('/remove_manager/{id}', 'DirectorController@showRemoveForm')->name('director.manager.show_remove');
    
    Route::put('/modify_manager/{id}', 'DirectorController@editManager')->name('director.manager.edit');
    Route::delete('/remove_manager/{id}', 'DirectorController@removeManager')->name('director.manager.remove');
    
    Route::get('/browse_requests', 'DirectorController@displayRequests')->name('director.requests.browse');
    Route::get('/request_more/{request_id}', 'DirectorController@readMoreRequest')->name('director.requests.read_more')->middleware('request.exist', 'approved.request');
    
    Route::put('/approve_request/{request_id}', 'DirectorController@approveRequest')->name('director.requests.approve');
    Route::put('/reject_request/{request_id}', 'DirectorController@rejectRequest')->name('director.requests.reject');
    
    Route::get('/removal_requests', 'DirectorController@displayStudentRemovalRequests')->name('director.student.removal_requests');
    
    Route::put('/approve_removal/{request_id}', 'DirectorController@approveStudentRemoval')->name('director.student.approve_removal');
    Route::put('/reject_removal/{request_id}', 'DirectorController@rejectStudentRemoval')->name('director.student.reject_removal');
    
    // send email to managers
    Route::get('/email_managers', 'DirectorController@showEmailManagerForm')->name('director.manager.email');
    Route::post('/email_managers', 'DirectorController@sendEmailManagers')->name('director.manager.send_email');
    // send email to staff (managers + lecturers)
    // send email to everybody (managers + lecturers + students)
    
    // profile
    Route::get('/retrieve_profiles/{id}', 'DirectorController@retrieveProfiles')->name('director.profiles.ajax');
    Route::get('/profile/{username}', 'DirectorController@showProfile')->name('director.profile.show')->middleware('request.ensure_profile');
    Route::get('/manager_profile/{username}', 'DirectorController@showManagerProfile')->name('director.manager.profile.show');
    Route::get('/lecturer_profile/{lecturer}', 'DirectorController@showLecturerProfile')->name('director.lecturer.profile.show');
    Route::get('/student_profile/{student}', 'DirectorController@showStudentProfile')->name('director.student.profile.show');
    
    // edit profile
    Route::get('/edit_profile', 'DirectorController@showEditProfile')->name('director.profile.showEdit');
    Route::put('/edit_profile', 'DirectorController@editProfile')->name('director.profile.edit');
    // All Profiles
    Route::get('/all_profiles', 'DirectorController@showAllProfiles')->name('director.profile.showAll');
    
    Route::get('/conversations', 'DirectorController@showConversations')->name('director.conversations.browse');
    Route::get('/conversations/{conversation}', 'DirectorController@showConversation')->name('director.show.conversation');
    Route::post('/conversations/send_message/{conversation}', 'DirectorController@sendMessage')->name('director.message.send');
    
    Route::get('/conversation/new', 'DirectorController@showNewConversationForm')->name('director.conversation.new');
    Route::post('/conversation/new', 'DirectorController@startNewConversation')->name('director.conversation.start');
});

Route::group(['prefix' => '/manager'], function() {
    Route::get('/login', 'ManagerAuth\AuthenticatesManagers@showLoginPage')->name('manager.show');
    Route::post('/login', 'ManagerAuth\AuthenticatesManagers@login')->name('manager.login');
    Route::get('/dashboard', 'ManagerController@showDashboard')->name('manager.dashboard');
    Route::get('/logout', 'ManagerAuth\AuthenticatesManagers@logout')->name('manager.logout');
    
    Route::get('/add_group', 'ManagerController@showGroupForm')->name('manager.group.add');
    Route::post('/add_group', 'ManagerController@addGroup')->name('manager.group.insert');
    
    Route::get('/add_lecturer', 'ManagerController@showLecturerForm')->name('manager.lecturer.add');
    Route::post('/add_lecturer', 'ManagerController@addLecturer')->name('manager.lecturer.insert');
    
    Route::get('/add_student', 'ManagerController@showStudentForm')->name('manager.student.add');
    Route::post('/add_student', 'ManagerController@addStudent')->name('manager.student.insert');
    
    Route::get('/make_request', 'ManagerController@showRequestForm')->name('manager.director.make_request');
    Route::post('/make_request', 'ManagerController@makeRequest')->name('manager.director.request');
    
    Route::get('/browse_lecturers', 'ManagerController@displayLecturers')->name('manager.lecturer.browse');
    // additional lecturer actions
    
    Route::get('/browse_requests', 'ManagerController@displayRequests')->name('manager.requests.browse');
    Route::get('/read_more/{request_id}', 'ManagerController@readMoreRequest')->name('manager.requests.read_more')->middleware('request.exist', 'request.users');
    Route::delete('/delete_request/{request_id}', 'ManagerController@deleteRequest')->name('manager.requests.remove');
    
    Route::get('/browse_classes', 'ManagerController@displayClassForm')->name('manager.classes.browse');
    Route::get('/class_list/{id}', 'ManagerController@retrieveClasses')->name('ajaxClass');
    Route::get('/student_list/{id}', 'ManagerController@retrieveStudents')->name('ajaxHelper');
    
    Route::get('/remove_student/{student_id}', 'ManagerController@displayStudentRemovalForm')->name('manager.student.show_remove');
    Route::post('/remove_student/{student_id}', 'ManagerController@makeStudentRemoveRequest')->name('manager.student.make_remove_request');
    
    Route::get('/removal_requests', 'ManagerController@displayStudentRemovalRequests')->name('manager.student.removal_requests');
    
    Route::put('/confirm_removal/{request_id}', 'ManagerController@confirmStudentRemoval')->name('manager.requests.confirm_removal');
    Route::put('/dispose_removal/{request_id}', 'ManagerController@disposeStudentRemoval')->name('manager.requests.dispose_removal');
    
    Route::get('/profile/{username}', 'ManagerController@showProfile')->name('manager.profile.show')->middleware('request.ensure_profile');
    Route::get('/edit_profile', 'ManagerController@showEditProfile')->name('manager.profile.showEdit');
    Route::put('/edit_profile', 'ManagerController@editProfile')->name('manager.profile.edit');
    
    Route::get('/conversations', 'ManagerController@showConversations')->name('manager.conversations.browse');
    Route::post('/conversations/{conversation}', 'ManagerController@sendMessage')->name('manager.message.send');
});

Route::group(['prefix' => '/lecturer'], function() {
    Route::get('/login', 'LecturerAuth\AuthenticatesLecturers@showLoginPage')->name('lecturer.show');
    Route::post('/login', 'LecturerAuth\AuthenticatesLecturers@login')->name('lecturer.login');
    Route::get('/dashboard', 'LecturerController@showDashboard')->name('lecturer.dashboard');
    Route::get('/logout', 'LecturerAuth\AuthenticatesLecturers@logout')->name('lecturer.logout');
    
    Route::get('/profile/{lecturer}', 'LecturerController@showProfile')->name('lecturer.profile.show');
});

Route::group(['prefix' => '/student'], function() {
    Route::get('/login', 'StudentAuth\AuthenticatesStudents@showLoginPage')->name('student.show');
    Route::post('/login', 'StudentAuth\AuthenticatesStudents@login')->name('student.login');
    Route::get('/dashboard', 'StudentController@showDashboard')->name('student.dashboard');
    Route::get('/logout', 'StudentAuth\AuthenticatesStudents@logout')->name('student.logout');
    
    Route::get('/profile/{student}', 'StudentController@showProfile')->name('student.profile.show');
});
