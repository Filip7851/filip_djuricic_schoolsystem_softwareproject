function retrieveClassStudents(id)
{    
    let items=[];
    $.get('/manager/student_list/'+id, function(data) {
        $('#present').show();
        $.each(data, function(index, studentObj) {
            items.push('<tr>');
            items.push('<td>' + studentObj.student_id + '</td>');
            items.push('<td>' + studentObj.first_name + ' ' + studentObj.last_name + '</td>');
            items.push('<td>' + studentObj.email + '</td>');
            items.push('<td>' + studentObj.username + '</td>');
            let gender = 0;
            if(studentObj.gender === 0)
            {
                gender = 'Male';
            }
            else
            {
                gender = 'Female';
            }
            items.push('<td>' + gender + '</td>');
            
            items.push('<td>' + '<a href="/manager/remove_student/'+studentObj.student_id+'" class="btn btn-danger">Remove</a>' + '</td>');
            items.push('</tr>');
        }); 
        $('<tbody/>', {html: items.join("")}).appendTo('#present');
    });
}

$(document).ready(function() {
    $('#groups').on('change', function(e) {
        // hide until query
        $('#present').hide();
        
        $('#present').find('tr:gt(0)').remove();
        
        var id = e.target.value;
        
        retrieveClassStudents(id);
    });
    
    // FINISH up
    $('#levels').on('change', function(e) {
        $('#present').hide();
        $('#present').find('tr:gt(0)').remove();
        var level_id = e.target.value;
        $('#groups').html('');
        let class_id = 0;
        
        $.get('/manager/class_list/'+level_id, function(data) {
          $.each(data, function(index, groupObj) {
            $('#groups').append("<option value="+groupObj.group_id+">" + groupObj.group_name + "</option>");
            class_id = $('#groups').val();
          });
          
          retrieveClassStudents(class_id);
        });
    });
});