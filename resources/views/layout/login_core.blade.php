<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/sweetalert2.min.css">
    <!-- Custom Theme Style -->
    <link href="../css/style.css" rel="stylesheet">
    
    <script src="../../js/sweetalert2.min.js"></script>
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="@yield('action')" method="post">
              <h1>@yield('login_title')</h1>
              <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                @if ($errors->has('username'))
                  <div class="text-left">
                    <strong style="color: red">{{ $errors->first('username') }}</strong>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                @if ($errors->has('password'))
                  <div class="text-left">
                    <strong style="color: red">{{ $errors->first('password') }}</strong>
                  </div>
                @endif
              </div>
              @if($errors->has('fail'))
                <div class="text-left">
                  <strong style="color: red">{{ $errors->first('fail') }}</strong>
                </div>
              @endif
              {{ csrf_field() }}
              <div>
                <button class="btn btn-block btn-default" type="submit" id="submit">Login</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div>
                  @yield('login_links')
                  <p>©2017 All Rights Reserved</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
    
    @if(session()->has('unauthorized'))
      <script>
        swal(
          'Unauthorized',
          'You are unauthorized to see this page. Please login first.',
          'error'
        );
      </script>
    @endif
  </body>
</html>
