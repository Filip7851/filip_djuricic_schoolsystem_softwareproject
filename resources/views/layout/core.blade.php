<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>@yield('title')</title>
        
        <!-- Bootstrap -->
        <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
        <!-- iCheck -->
        <link href="../../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        
        <!-- bootstrap-progressbar -->
        <link href="../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="../../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <!-- <link href="../../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="../../css/sweetalert2.min.css">
    
        <link rel="stylesheet" href="../../css/style.css">
        
        <script src="../../js/sweetalert2.min.js"></script>
    </head>
    
    <body class="nav-md">
    
        <div class="container body">
            @include('layout.partials.nav')
            <!-- Include top menu -->
            @include('layout.partials.top_menu')
            
            @yield('content')
            
            @include('layout.partials.footer')
        </div>
        
        <!-- SCRIPTS -->
        <!-- jQuery -->
        <script src="../../vendors/jquery/dist/jquery.min.js"></script>
        @yield('ajaxCalls')
        <!-- Bootstrap -->
        <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../../vendors/fastclick/lib/fastclick.js"></script>
        <!-- gauge.js -->
        <script src="../../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- Skycons -->
        <script src="../../vendors/skycons/skycons.js"></script>
        <!-- iCheck -->
        <script src="../../vendors/iCheck/icheck.min.js"></script>
        <!-- Flot -->
        <script src="../../vendors/Flot/jquery.flot.js"></script>
        <script src="../../vendors/Flot/jquery.flot.pie.js"></script>
        <script src="../../vendors/Flot/jquery.flot.time.js"></script>
        <script src="../../vendors/Flot/jquery.flot.stack.js"></script>
        <script src="../../vendors/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="../../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="../../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="../../vendors/flot.curvedlines/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="../../vendors/DateJS/build/date.js"></script>
        <!-- JQVMap -->
        <script src="../../vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="../../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="../../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../../vendors/moment/min/moment.min.js"></script>
        @yield('datatables_scripts')
        
        <!-- Custom JS -->
        <script src="../../js/script.min.js"></script> 
        @if(session()->has('authorized'))
            <script>
                swal(
                    'Already authorized',
                    'You can\'t see this page since you are already authorized.',
                    'error'
                );
            </script>
        @elseif(session()->has('unauthorized'))
            <script>
                swal(
                    'Unauthorized',
                    'You are not authorized to see this page.',
                    'error'  
                );
            </script>
        @endif
        @if(session()->has('already_solved'))
            <script>
                swal(
                    'Error',
                    'That request has either been solved, or doesn\'t exist.',
                    'error'
                );
            </script>
        @endif
        @if(session()->has('no_request'))
            <script>
                swal(
                    'Error',
                    'That request does not exist or has been removed.',
                    'error'
                );
            </script>
        @endif
        @if(session()->has('req_not_users'))
            <script>
                swal(
                    'Error',
                    'That request is not yours so you can\'t browse it.',
                    'error'
                );
            </script>
        @endif
    </body>
</html>
