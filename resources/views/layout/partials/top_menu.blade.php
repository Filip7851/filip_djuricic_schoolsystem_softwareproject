<!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="../../images/img.jpg" alt="">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    @if(Auth::guard('director')->check())
                      <li><a href="{{ route('director.profile.show', ['username' => Auth::user()->username]) }}"> Profile</a></li>
                    @elseif(Auth::guard('manager')->check())
                      <li><a href="{{ route('manager.profile.show', ['username' => Auth::user()->username]) }}"> Profile</a></li>
                    @elseif(Auth::guard('lecturer')->check())
                      <li><a href="{{ route('lecturer.profile.show', ['profile_id' => Auth::user()->lecturer_id]) }}"> Profile</a></li>
                    @elseif(Auth::guard('student')->check())
                      <li><a href="{{ route('student.profile.show', ['profile_id' => Auth::user()->student_id]) }}"> Profile</a></li>
                    @endif
                    @if(Auth::guard('director')->check())
                      <li><a href="{{ route('director.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    @elseif(Auth::guard('manager')->check())
                      <li><a href="{{ route('manager.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    @elseif(Auth::guard('lecturer')->check())
                      <li><a href="{{ route('lecturer.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    @elseif(Auth::guard('student')->check())
                      <li><a href="{{ route('student.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    @endif
                    
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  @if(Auth::guard('director')->check())
                    <a href="{{ route('director.requests.browse') }}" class="dropdown-toggle info-number" title="You have {{ App\Demand::where('approved', '!=', '-1')->where('approved', '!=', '1')->where('student_id', null)->where('lecturer_id', null)->count() }} requests pending">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">{{ App\Demand::where('approved', '!=', '-1')->where('approved', '!=', '1')->where('student_id', null)->where('lecturer_id', null)->count() }}</span>
                    </a>
                  @elseif(Auth::guard('manager')->check())
                    <a href="{{ route('manager.requests.browse') }}" class="dropdown-toggle info-number" title="You have {{ App\Demand::where('manager_id', Auth::user()->id)->where('approved', 0)->where('student_id', null)->where('lecturer_id', null)->count() }} requests pending">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">{{ App\Demand::where('manager_id', Auth::user()->id)->where('approved', 0)->where('student_id', null)->where('lecturer_id', null)->count() }}</span>
                    </a>
                  @endif
                </li>
                
                <li role="presentation" class="dropdown">
                  @if(Auth::guard('director')->check())
                      <a href="{{ route('director.student.removal_requests') }}" class="dropdown-toggle info-number" title="You have {{ App\Demand::all()->where('approved', '!=', '-1')->where('approved', '!=', '1')->where('student_id', '!=', null)->count() }} student removal requests pending">
                        <i class="fa fa-times"></i>
                        <span class="badge bg-green">{{ App\Demand::where('approved', '!=', '-1')->where('approved', '!=', '1')->where('student_id', '!=', null)->count() }}</span>
                      </a>
                  @elseif(Auth::guard('manager')->check())
                      <a href="{{ route('manager.student.removal_requests') }}" class="dropdown-toggle info-number" title="You have {{ App\Demand::where('request_title', 'Student Removal')->where('manager_id', Auth::user()->id)->count() }} student removal requests pending">
                        <i class="fa fa-times"></i>
                        <span class="badge bg-green">{{ App\Demand::where('request_title', 'Student Removal')->where('manager_id', Auth::user()->id)->count() }}</span>
                      </a>
                  @endif
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
