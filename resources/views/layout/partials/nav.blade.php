      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../../images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                @if(Auth::guard('director')->check())
                  <h2><a href="{{ route('director.profile.show', ['director' => Auth::user()->id]) }}" style="color: white" title="View Your Profile">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</a></h2>
                @elseif(Auth::guard('manager')->check())
                  <h2><a href="{{ route('manager.profile.show', ['manager' => Auth::user()->id]) }}" style="color: white" title="View Your Profile">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</a></h2>
                @elseif(Auth::guard('lecturer')->check())
                  <h2><a href="{{ route('lecturer.profile.show', ['lecturer' => Auth::user()->lecturer_id]) }}" style="color: white" title="View Your Profile">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</a></h2>
                @elseif(Auth::guard('student')->check())
                  <h2><a href="{{ route('student.profile.show', ['student' => Auth::user()->student_id]) }}" style="color: white" title="View Your Profile">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</a></h2>
                @endif
                @if(Auth::guard('manager')->check() || Auth::guard('lecturer')->check())
                  <span>Your Department:</span>
                  <h2>
                    @if(isset($man_dept))
                      {{ $man_dept }}
                    @endif
                  </h2>
                @elseif(Auth::guard('lecturer')->check())
                  <span>Your Department:</span>
                  <h2>
                    {{ $lec_dept }}
                  </h2>
                @elseif(Auth::guard('student')->check())
                  <span>Your Department:</span>
                  <h2>
                    {{ $lec_dept }}
                  </h2>
                @endif
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      @if(Auth::guard('director')->check())
                        <li><a href="{{ route('director.dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ route('director.profile.show', ['username' => Auth::user()->username]) }}">Profile Overview</a></li>   
                      @elseif(Auth::guard('manager')->check())
                        <li><a href="{{ route('manager.dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ route('manager.profile.show', ['username' => Auth::user()->username]) }}">Profile Overview</a></li>
                      @elseif(Auth::guard('lecturer')->check())
                        <li><a href="{{ route('lecturer.dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ route('lecturer.profile.show', ['profile_id' => Auth::user()->lecturer_id]) }}">Profile Overview</a></li>
                      @elseif(Auth::guard('student')->check())
                        <li><a href="{{ route('student.dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ route('student.profile.show', ['profile_id' => Auth::user()->lecturer_id]) }}">Profile Overview</a></li>
                      @endif
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Add New <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      @if(Auth::guard('director')->check())
                        <li><a href="{{ route('director.manager.add') }}">Manager</a></li>
                      @elseif(Auth::guard('manager')->check())
                        <li><a href="{{ route('manager.group.add') }}">Group</a></li>
                        <li><a href="{{ route('manager.lecturer.add') }}">Lecturer</a></li>
                        <li><a href="{{ route('manager.student.add') }}">Student</a></li>
                        <li><a href="{{ route('manager.director.make_request') }}">Request</a></li>
                      @endif
                    </ul>
                  </li>
                  <li><a><i class="fa fa-folder-open"></i> Browse <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      @if(Auth::guard('director')->check())
                        <li><a href="{{ route('director.manager.browse') }}">Managers</a></li>
                        <li><a href="{{ route('director.requests.browse') }}">Requests</a></li>
                        <li><a href="{{ route('director.profile.showAll') }}">All Profiles</a></li> 
                        <li><a href="{{ route('director.conversations.browse') }}">Conversations</a></li>
                      @elseif(Auth::guard('manager')->check())
                        <li><a href="{{ route('manager.lecturer.browse') }}">Lecturers</a></li>
                        <li><a href="{{ route('manager.classes.browse') }}">Classes</a></li>
                        <li><a href="{{ route('manager.requests.browse') }}">Requests</a></li>
                        <li><a href="{{ route('manager.student.removal_requests') }}">Removal Requests</a></li>
                        <li><a href="{{ route('manager.conversations.browse') }}">Conversations</a></li>
                      @endif
                    </ul>
                  </li>
                  @if(Auth::guard('director')->check())
                  <li><a><i class="fa fa-envelope"></i> Send Email <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('director.manager.email') }}">To Managers</a></li>
                    </ul>
                  </li>
                  @endif
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              @if(Auth::guard('director')->check())
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('director.logout')}}">
                  <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
              @elseif(Auth::guard('manager')->check())
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('manager.logout')}}">
                  <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
              @elseif(Auth::guard('lecturer')->check())
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('lecturer.logout')}}">
                  <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
              @elseif(Auth::guard('student')->check())
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('student.logout')}}">
                  <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
              @endif
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
