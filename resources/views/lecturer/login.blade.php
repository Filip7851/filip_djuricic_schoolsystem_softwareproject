@extends('layout.login_core')

@section('title', 'Lecturer Login Page')

@section('login_title', 'Lecturer Login')
@section('action', '/lecturer/login')
@section('login_links')
    <h2><strong><a href="{{ route('director.show') }}">Director Login</a></strong></h2>
    <h2><strong><a href="{{ route('manager.show') }}">Manager Login</a></strong></h2>
    <h2><strong><a href="{{ route('student.show') }}">Student Login</a></strong></h2>
@endsection