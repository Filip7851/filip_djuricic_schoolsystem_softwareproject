@component('mail::message')
# New email from director

{{ $messageContent }}

Thanks,<br>
The Director <br>
{{ config('app.name') }}
@endcomponent
