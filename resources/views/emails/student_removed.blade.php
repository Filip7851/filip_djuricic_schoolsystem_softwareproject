@component('mail::message')
# You have been removed from the system

<i>Dear {{ $studentName }},</i><br><br>

Following the request of {{ $managerName }}, you have been removed from our system.

Thanks,<br>
The Director<br>
{{ config('app.name') }}
@endcomponent
