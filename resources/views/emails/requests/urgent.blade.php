@component('mail::message')
# You have urgent request pending

You have the request which is titled {{ $requestTitle }} marked as urgent by <strong>{{ $managerName }}</strong>. <br>  
Please log in as soon as possible and resolve it.

Thanks,<br>
{{ config('app.name') }}
@endcomponent