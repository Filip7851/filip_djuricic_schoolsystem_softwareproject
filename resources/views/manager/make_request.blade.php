@extends('layout.core')

@section('title', 'Make Request To The Director')

@section('content')
    <div class="row">
        <div class="right_col" role="main">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Make Request</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <form id="demo-form2" method="POST" action="{{ route('manager.director.request') }}" action="POST" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="request_title">Request Title</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="request_title" name="request_title" class="form-control col-md-7 col-xs-12">
                                    @if($errors->has('request_title'))
                                      <div class="text-left">
                                        <strong style="color: red">{{ $errors->first('request_title') }}</strong>
                                      </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="request_body">Request Body</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea id="request_body" class="form-control" name="request_body"></textarea>
                                    @if($errors->has('request_body'))
                                      <div class="text-left">
                                        <strong style="color: red">{{ $errors->first('request_body') }}</strong>
                                      </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="request_body">Is Urgent?</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" name="is_urgent" value="1"> Check ONLY if the request is urgent.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="text-right">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-success" type="submit">Make Request</button>
                                    </div>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>    
        </div>
    </div>
@endsection