@extends ('layout.core')

@section('title', 'Manager Dashboard')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Users (in Dept.)</span>
              <div class="count" id="tot_use_dept">{{ $totalUsers }}</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>Possible Statistics</i></span> -->
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top">Total Lecturers (in Dept.)</span>
              <div class="count" id="tot_lec_dept">{{ $totalLecturers }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top">Total Classes (in Dept.)</span>
              <div class="count" id="tot_gro_dept">{{ $totalGroups }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top">Total Students (in Dept.)</span>
              <div class="count green" id="tot_stu_dept">{{ $totalStudents }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top">Male Students (in Dept.)</span>
              <div class="count" id="tot_m_stu_dept">{{ $totalMaleStudents }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top">Female Students (in Dept.)</span>
              <div class="count" id="tot_f_stu_dept">{{ $totalFemaleStudents }}</div>
            </div>
          </div>
          <!-- /top tiles -->
          
          <!-- add tiles -->
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Add new class</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To add new class to this department, please click the button bellow</h4>
                
                    <a class="btn btn-block btn-success" href="{{route('manager.group.add')}}">Add New</a>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2>Add new lecturer</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To add new lecturer to this department, please click the button below</h4>
                    
                    <a class="btn btn-block btn-success" href="{{ route('manager.lecturer.add') }}">Add New</a>
                </div>
              </div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Add new student</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To add new student to a class, please click button bellow</h4>
                    
                    <a class="btn btn-block btn-success" href="{{ route('manager.student.add') }}">Add New</a>
                </div>
            </div>
          </div>
          </div>
          <!-- /add tiles -->
          
          <!-- browse tiles -->
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Browse classes</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To browse all classes in this department click the button below</h4>
                
                    <a class="btn btn-block btn-success" href="{{route('manager.classes.browse')}}">Browse</a>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2>Browse lecturers</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To browse all lecturers in this department click the button below</h4>
                    
                    <a class="btn btn-block btn-success" href="{{ route('manager.lecturer.browse') }}">Browse</a>
                </div>
              </div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Browse requests</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To browse your requests click the button below</h4>
                    
                    <a class="btn btn-block btn-success" href="{{ route('manager.requests.browse') }}">Browse</a>
                </div>
            </div>
          </div>
          </div>
          <!-- /browse tiles -->
@endsection
