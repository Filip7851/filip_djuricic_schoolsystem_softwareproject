@extends('layout.login_core')

@section('title', 'Manager Login Page')

@section('login_title', 'Manager Login')
@section('action', '/manager/login')
@section('login_links')
    <h2><strong><a href="{{ route('director.show') }}">Director Login</a></strong></h2>
    <h2><strong><a href="{{ route('lecturer.show') }}">Lecturer Login</a></strong></h2>
    <h2><strong><a href="{{ route('student.show') }}">Student Login</a></strong></h2>
@endsection