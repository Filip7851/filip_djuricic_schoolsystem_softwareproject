@extends('layout.core')

@section('title', 'Manager Profile Overview')

@section('content')
@if(session()->has('profile_modified'))
  <script>
    swal(
      'Modified',
      'Successfully Modified Profile',
      'success'
    );
  </script>
@endif
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>User Profile</h3>
      </div>
    </div>
    
    <div class="clearfix"></div>
    
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>{{ $manager->first_name }}'s Profile Overview</h2>
            
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
              <div class="profile_img">
                <div id="crop-avatar">
                  <!-- Current avatar -->
                  <img class="img-responsive avatar-view" src="../../images/picture.jpg" alt="Avatar" title="Change the avatar">
                </div>
              </div>
              
              <h3>{{ $manager->first_name.' '.$manager->last_name }}</h3>

              <ul class="list-unstyled user_data">
                <li title="Username">
                  <i class="fa fa-user user-profile-icon"></i> {{ $manager->username }}
                </li>
                
                <li title="Email">
                  <i class="fa fa-envelope user-profile-icon"></i> {{ $manager->email }}
                </li>

                <li title="Role">
                  <i class="fa fa-briefcase user-profile-icon"></i> Manager
                </li>
                
                <li title="Department">
                  <i class="fa fa-building-o"></i> {{ $manager->department->department_name }}
                </li>

                <li title="ID Card">
                    <i class="fa fa-id-card" aria-hidden="true"></i> {{ $manager->id_card }}
                </li>
              </ul>
              
              @if(isset($displayEdit))
                <a class="btn btn-success" href="{{ route('manager.profile.showEdit') }}"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
              @endif
              <br>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="profile_title">
                <div class="col-md-6">
                  <h2>User Activity Report</h2>
                </div>
              </div>

              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                    <!-- start recent activity -->
                    <ul class="messages">
                      @foreach($activities as $activity)
                          <li>
                            <img src="../../images/img.jpg" class="avatar" alt="Avatar">
                            <div class="message_wrapper">
                              <h4 class="heading">{{ $activity->manager->first_name.' '.$activity->manager->last_name }}</h4>
                              <blockquote class="message">{{ $activity->activity_content }}</blockquote>
                              <br />
                              <p class="url">
                                <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                <a href="#"><i class="fa fa-clock-o"></i> {{ $activity->created_at->diffForHumans() }} </a>
                              </p>
                            </div>
                          </li>
                      @endforeach
                    </ul>
                    <div class="text-right">
                      {!! $activities->links() !!}
                    </div>
                    <!-- end recent activity -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection