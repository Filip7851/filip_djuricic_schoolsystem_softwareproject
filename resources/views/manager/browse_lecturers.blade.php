@extends('layout.core')

@section('title', 'Browse Lecturers')

@section('content')
@if(session()->has('lecturer_inserted'))
  <script>
    swal(
      'Inserted',
      'Successfully Inserted Lecturer',
      'success'
    );
  </script>
@endif
@if(session()->has('lecturer_removed', ''))
  <script>
    swal(
        'Removed',
        'Successfully Removed Request',
        'success'
    );
  </script>
@endif
    <div class="right_col" role="main">
        <div class="clearfix"></div>
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{{ $man_dept }} Department Lecturers</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    @if(count($lecturers) == 0)
                        <strong>Currently, there are no registered lecturers in {{ $man_dept }} department.</strong><br><br>
                        <p>To insert new lecturer, <a href="{{ route('manager.lecturer.add') }}" style="text-decoration: underline">click here</a>.</p>
                    @else
                    <p>Here, you can view all lecturers registered in this department.</p>
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Lecturer ID</th>
                            <th class="column-title">Lecturer Name</th>
                            <th class="column-title">Lecturer Email</th>
                            <th class="column-title">Lecturer Username</th>
                            <th class="column-title">Lecturer Gender</th>
                            <th class="column-title no-link last"><span class="nobr">Lecturer ID Card</span>
                            
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($lecturers as $lecturer)
                                    @if($lecturer->lecturer_id % 2 == 0)
                                        <tr class="even pointer">
                                            <td>{{ $lecturer->lecturer_id }}</td>
                                            <td>{{ $lecturer->first_name.' '.$lecturer->last_name }}</td>
                                            <td>{{ $lecturer->email }}</td>
                                            <td>{{ $lecturer->username }}</td>
                                            <td>
                                                @if($lecturer->gender == 0)
                                                    Male
                                                @elseif($lecturer->gender == 1)
                                                    Female
                                                @endif
                                            </td>
                                            <td>{{ $lecturer->id_card }}</td>
                                        </tr>
                                    @else
                                        <tr class="odd pointer">
                                            <td>{{ $lecturer->lecturer_id }}</td>
                                            <td>{{ $lecturer->first_name.' '.$lecturer->last_name }}</td>
                                            <td>{{ $lecturer->email }}</td>
                                            <td>{{ $lecturer->username }}</td>
                                            <td>
                                                @if($lecturer->gender == 0)
                                                    Male
                                                @elseif($lecturer->gender == 1)
                                                    Female
                                                @endif
                                            </td>
                                            <td>{{ $lecturer->id_card }}</td>
                                        </tr>
                                    @endif
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection