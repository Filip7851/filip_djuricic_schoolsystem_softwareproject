@extends('layout.core')

@section('title', 'Add New Group')

@section('content')
<div class="row">
  <div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Add New Group</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form id="demo-form2" method="POST" action="{{ route('manager.group.insert') }}" data-parsley-validate class="form-horizontal form-label-left">

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prefix">Prefix
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="prefix" name="prefix" class="form-control col-md-7 col-xs-12" value="{{ $man_dept_short }}" disabled>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="level">Level
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="level" class="form-control">
                    <option value="4">4</option>
                    <option value="6">6</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="class_no" class="control-label col-md-3 col-sm-3 col-xs-12">Class No.</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="class_no" class="form-control col-md-7 col-xs-12" type="text" name="class_no">
                @if($errors->has('class_no'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('class_no') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Department
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="department_id" class="form-control" disabled>
                    <option value="">{{ $man_dept }}</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="text-right">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="reset">Reset Form</button>
                <button type="submit" id="submit" class="btn btn-success">Add</button>
              </div>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection