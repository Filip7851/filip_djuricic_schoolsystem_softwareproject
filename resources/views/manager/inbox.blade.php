@extends('layout.core')

@section('title', 'Conversation Overview')

@section('content')
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                          @if($conversation !== null)
                            <div class="col-sm-3 mail_list_column"> 
                              <!-- START NEW CONVERSATION?! -->  
                              <div class="x_title">
                                <h5>Your Conversations</h5>
                              </div>
                              <a href="#">
                                    <div class="mail_list">
                                        <div class="left">
                                            <i class="fa fa-circle"></i>
                                        </div>
                                        <div class="right">
                                          @if($lastMsgSentByDirector !== null)
                                            <h3>{{ $lastMsgSentByDirector->director->first_name.' '.$lastMsgSentByDirector->director->last_name }}</h3>
                                            <p>{{ $lastMsgSentByDirector->message_content }}</p>
                                          @else
                                            <h3>{{ Auth::user()->first_name.' '.Auth::user()->last_name }} to {{ App\Director::find(1)->first_name.' '.App\Director::find(1)->last_name }}</h3>
                                            <p>{{ $conversation->messages[0]->message_content }}</p>
                                          @endif
                                        </div>
                                    </div>
                                </a>
                            </div>
                          @endif
                      <!-- /MAIL LIST -->

                      <!-- CONTENT MAIL -->
                      <div class="col-sm-9 mail_view">
                        <div class="inbox-body">
                          <div class="mail_heading row">
                            <div class="col-md-8">
                                <div class="btn-group">
                                    <div class="btn-group">
                                        <a class="btn btn-sm btn-primary" type="button" href="#reply"><i class="fa fa-reply"></i> Reply</a>
                                      </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                
                            </div>
                            @if($conversation == null)
                                  <h3>You don't have any active conversations right now.</h3>
                                @else
                                  @foreach($messages as $message)
                                    @if($message->sent_by == 1)
                                      <div class="col-md-12">
                                        <h4>{{ $message->director->first_name.' '.$message->director->last_name }} to you - {{ $message->created_at->diffForHumans() }}</h4>
                                        <h5>{{ $message->message_content }}</h5>
                                      </div>
                                    @else
                                      <div class="col-md-12">
                                        <h4><a href="{{ route('manager.profile.show', ['username' => Auth::user()->username]) }}" title="View Your Profile">{{ Auth::user()->first_name.' '.Auth::user()->last_name }} - {{ $message->created_at->diffForHumans() }}</a></h4>
                                        <h5>{{ $message->message_content }}</h5>
                                      </div>
                                    @endif
                                @endforeach
                                @endif
                            <div class="col-md-12">
                                <!-- SHOULD BE CONVO TITLE -->
                            </div>
                          </div>
                          <div class="sender-info">
                            <div class="row">
                              <div class="col-md-12">
                                <hr>
                                <div class="text-right">
                                  {!! $messages->links() !!}
                                </div>
                                <form action="{{ route('manager.message.send', ['conversation' => $conversation]) }}" method="post">
                                    <textarea name="reply_msg" id="reply_msg" class="form-control" placeholder="Reply"></textarea>
                                    @if ($errors->has('reply_msg'))
                                      <strong style="color: red">{{ $errors->first('reply_msg') }}</strong>
                                    @endif
                                    
                                    <hr>
                                    <div class="text-right">
                                      <button class="btn btn-primary" type="submit">Reply</button>
                                    </div>
                                    {{ csrf_field() }}
                                </form>
                              </div>
                            </div>
                          </div>
                          <div class="view-mail">
                            <!-- CONTENT OF MSG -->
                          </div>
                        </div>
                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection