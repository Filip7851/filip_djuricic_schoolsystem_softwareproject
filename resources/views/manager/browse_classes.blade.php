@extends('layout.core')

@section('title', 'Browse Classes')

@section('content')
@if(session()->has('group_inserted'))
  <script>
    swal(
      'Inserted',
      'Successfully Created Group',
      'success'
    );
  </script>
@endif
@if(session()->has('student_inserted'))
  <script>
    swal(
      'Inserted',
      'Successfully Inserted Student',
      'success'
    );
  </script>
@endif
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ $man_dept }} Classes</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                    <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if($class_count_by_dept > 0)
                            <h3>Please select the class from dropdown that you want to browse.</h3>
                            <select name="levels" id="levels" class="form-control">
                                <option value="4">4</option>
                                <option value="6">6</option>
                            </select>
                            <select name="groups" id="groups" class="form-control" style="margin-top: 20px">
                                @foreach($allGroups as $group)
                                    <option value="{{ $group->group_id }}">{{ $group->group_name }}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <div class="table-responsive">
                              <table id="present" class="table table-striped jambo_table">
                                <thead>
                                  <tr class="headings">
                                    <th class="column-title">Student ID</th>
                                    <th class="column-title">Student Name</th>
                                    <th class="column-title">Student Email</th>
                                    <th class="column-title">Student Username</th>
                                    <th class="column-title">Student Gender</th>
                                    <th class="column-title no-link last"><span class="nobr">Remove Student</span></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($classStudents as $student)
                                      <tr>
                                        <td>{{ $student->student_id }}</td>
                                        <td>{{ $student->first_name.' '.$student->last_name }}</td>
                                        <td>{{ $student->email }}</td>
                                        <td>{{ $student->username }}</td>
                                        <td>@if($student->gender == 0) Male @else Female @endif</td>
                                        <td><a href="{{ route('manager.student.show_remove', ['student_id' => $student->student_id]) }}" class="btn btn-danger">Remove</a></td>
                                      </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                        @else
                            <h3>No Classes in this Dept</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('ajaxCalls')
<script src="../js/retrieve_classes.min.js"></script>
@endsection
