@extends('layout.core')

@section('title', 'Add New Student')

@section('content')
    @if($class_count_by_dept <= 0)
    <!-- Error Message -->
    <div class="row">
        <div class="right_col" role="main">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x-panel">
                    <h2>No Classes</h2>
                </div>
                <div class="x-content">
                    <h3>There is no classes registered for your department.<br> To add class click <a href="{{ route('manager.group.add') }}">here.</a></h3>          
                </div>
            </div>
        </div>
    </div>
    @else
        <div class="row">
          <div class="right_col" role="main">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Add New Student</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form id="demo-form2" method="POST" action="{{ route('manager.student.insert') }}" data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first_name">First Name
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first_name" name="first_name" class="form-control col-md-7 col-xs-12">
                        @if($errors->has('first_name'))
                          <div class="text-left">
                              <strong style="color: red">{{ $errors->first('first_name') }}</strong>
                          </div>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_name">Last Name
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="last_name" name="last_name" class="form-control col-md-7 col-xs-12">
                        @if($errors->has('last_name'))
                          <div class="text-left">
                              <strong style="color: red">{{ $errors->first('last_name') }}</strong>
                          </div>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gender">Gender</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="gender" class="btn-group" data-toggle="buttons">
                            <button class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                <input type="radio" name="gender"value="0"> &nbsp; Male &nbsp;
                            </button>
                            <button class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                <input type="radio" name="gender" value="1"> Female
                            </button>
                        </div>
                        @if($errors->has('gender'))
                            <br><br>
                            <div class="text-left">
                                <strong style="color: red">{{ $errors->first('gender') }}</strong>
                            </div>
                        @endif
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="email" class="form-control col-md-7 col-xs-12" type="email" name="email">
                        @if($errors->has('email'))
                          <div class="text-left">
                              <strong style="color: red">{{ $errors->first('email') }}</strong>
                          </div>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="id_card" class="control-label col-md-3 col-sm-3 col-xs-12">ID Card</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="id_card" class="form-control col-md-7 col-xs-12" type="id_card" name="id_card">
                        @if($errors->has('id_card'))
                          <div class="text-left">
                              <strong style="color: red">{{ $errors->first('id_card') }}</strong>
                          </div>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Group</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                      <select name="group" class="form-control">
                        <!-- GROUPS FOR THIS DEPT -->
                        @foreach($groups as $group)
                            <option value="{{ $group->group_id }}">{{ $group->group_name }}</option>
                        @endforeach
                      </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="text-right">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button class="btn btn-primary" type="reset">Reset Form</button>
                        <button type="submit" class="btn btn-success">Add</button>
                      </div>
                      </div>
                    </div>
                    {{ csrf_field() }}
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    @endif
@endsection
