@extends('layout.core')

@section('title', 'Add New Lecturer')

@section('content')
<div class="row">
  <div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Add New Lecturer</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br>
          <form id="demo-form2" method="POST" action="{{ route('manager.lecturer.insert') }}" data-parsley-validate class="form-horizontal form-label-left">

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first_name">First Name
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="first_name" name="first_name" class="form-control col-md-7 col-xs-12">
                @if($errors->has('first_name'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('first_name') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_name">Last Name
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="last_name" name="last_name" class="form-control col-md-7 col-xs-12">
                @if($errors->has('last_name'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('last_name') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div id="gender" class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                    <input type="radio" name="gender" value="0" checked> &nbsp; Male &nbsp;
                  </label>
                  <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                    <input type="radio" name="gender" value="1"> Female
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="email" class="form-control col-md-7 col-xs-12" type="email" name="email">
                @if($errors->has('email'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('email') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label for="id_card" class="control-label col-md-3 col-sm-3 col-xs-12">ID Card</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="id_card" class="form-control col-md-7 col-xs-12" type="id_card" name="id_card">
                @if($errors->has('id_card'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('id_card') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="text-right">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="reset">Reset Form</button>
                <button type="submit" class="btn btn-success">Add</button>
              </div>
              </div>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection