@extends('layout.core')

@section('title', 'Read More About This Request')

@section('content')
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                          <div class="col-sm-3 mail_list_column">
                            <div class="x_title">
                              <h5>More from you</h5>
                            </div>
                            @foreach(\App\Demand::all()->where('manager_id', Auth::user()->id)->where('demand_id', '!=', $demand->demand_id) as $d)
                                <a href="{{ route('manager.requests.read_more', ['request_id' => $d->demand_id]) }}">
                                  <div class="mail_list">
                                      <div class="left">
                                          <i class="fa fa-circle"></i>
                                      </div>
                                      <div class="right">
                                          <h3>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h3>
                                          <p>{{ $d->request_body }}</p>
                                      </div>
                                  </div>
                                </a>
                              @endforeach
                          </div>
                      <!-- /MAIL LIST -->

                      <!-- CONTENT MAIL -->
                      <div class="col-sm-9 mail_view">
                        <div class="inbox-body">
                          <div class="mail_heading row">
                            <div class="col-md-8">
                                <strong>Status: {{ $status }}</strong>
                                @if($demand->approved != 0 && $demand->student_id == null)
                                <div class="btn-group">
                                    <div class="text-right">
                                      <form action="{{ route('manager.requests.remove', ['request_id' => $demand->demand_id]) }}" method="POST">
                                          <div class="form-group">
                                              <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-ban"></i> Remove</button>
                                          </div>
                                          {{ method_field('DELETE') }}
                                          {{ csrf_field() }}
                                      </form>
                                    </div>
                                </div>
                                @elseif($demand->approved == 1 && $demand->student_id != null)
                                <div class="btn-group">
                                    <div class="text-right">
                                      <form action="{{ route('manager.requests.confirm_removal', ['request_id' => $demand->demand_id]) }}" method="POST">
                                          <div class="form-group">
                                              <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-ban"></i> Confirm Removal</button>
                                          </div>
                                          {{ method_field('PUT') }}
                                          {{ csrf_field() }}
                                      </form>
                                    </div>
                                </div>
                                @elseif($demand->approved == -1 && $demand->student_id != null)
                                <div class="btn-group">
                                    <div class="text-right">
                                      <form action="{{ route('manager.requests.dispose_removal', ['request_id' => $demand->demand_id]) }}" method="POST">
                                          <div class="form-group">
                                              <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-ban"></i> Dispose</button>
                                          </div>
                                          {{ method_field('PUT') }}
                                          {{ csrf_field() }}
                                      </form>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-4 text-right">
                                <strong><p class="date">Request submitted {{ $demand->created_at->diffForHumans() }}</p></strong>
                            </div>
                            <div class="col-md-12">
                                <h4> {{ $demand->request_title }}</h4>
                            </div>
                          </div>
                          <div class="sender-info">
                            <div class="row">
                              <div class="col-md-12">
                                Request by
                                <strong>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</strong>
                                <span>({{ Auth::user()->email }})</span>
                              </div>
                            </div>
                          </div>
                          <div class="view-mail">
                            {{ $demand->request_body }}
                          </div>
                        </div>
                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
