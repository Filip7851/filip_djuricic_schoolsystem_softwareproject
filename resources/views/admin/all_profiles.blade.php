@extends('layout.core')

@section('title', 'All Profiles')

@section('content')
    <div class="right_col" role="main" style="min-height: 1168px;">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>View All Profiles</h3>
              </div> 
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <span class="input-group-btn">
                      <h4>Sort by:</h4>
                    </span>
                    <select name="roles" id="roles" class="form-control">
                      <option value="1">Managers</option>
                      <option value="2">Lecturers</option>
                      <option value="3">Students</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        <!-- SORTS -->
                      </div>

                      <div class="clearfix"></div>
                      
                      <div id="items">
                        @foreach($managers as $manager)
                          <div class="text_center">
                            <div class="col-md-4 col-sm-4 col-xs-12 profile_details text-center">
                              <div class="well profile_view">
                                <div class="col-sm-12">
                                  <h4 class="brief"><i> Position: Manager</i></h4>
                                  <div class="left col-xs-7">
                                    <h4><strong>{{ $manager->first_name.' '.$manager->last_name }}</strong></h4>
                                    <p><strong>ID Card: </strong> {{ $manager->id_card }}</p>
                                    <ul class="list-unstyled">
                                      <li><i class="fa fa-user"></i> Username: {{ $manager->username }} </li>
                                      <li><i class="fa fa-building"></i> Department: <span title="{{ $manager->department->department_name }}">{{ $manager->department->department_short }}</span></li>
                                    </ul>
                                  </div>
                                  <div class="right col-xs-5 text-center">
                                    <img src="../../images/img.jpg" alt="" class="img-circle img-responsive">
                                  </div>
                                </div>
                                <div class="col-xs-12 bottom text-center">
                                  <div class="col-xs-12 col-sm-6 emphasis">
                                  </div>
                                  <div class="col-xs-12 col-sm-6 emphasis">
                                    <a type="button" class="btn btn-primary btn-xs" href="{{ route('director.manager.profile.show', ['username' => $manager->username]) }}">
                                      <i class="fa fa-user"> </i> View Profile
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <!-- /row -->
            <!-- /items -->
          </div>
        </div>  
@endsection

@section('ajaxCalls')
<script type="text/javascript">
  $(document).ready(() => {
    $('#roles').val('1');
    var dpt = ['', 'Software Development', 'Multimedia Software Development', 'Computer Networks', 'End-User Support'];
    var dpt_short = ['', 'SWD', 'MSD', 'CSN', 'EUS'];
    
    $('#roles').on('change', (e) => {
      $('#items').html('');
      var id = e.target.value;
      
      $.get('/director/retrieve_profiles/'+id, (data) => {
        var role = data.role;
        if(role == 'Lecturer') {
          $.each(data.lecturers, (index, profileObj) => {
            let currRole = dpt[profileObj.department_id];
            let currRoleShr = dpt_short[profileObj.department_id];
            $('#items').append('<div class="text_center"><div class="col-md-4 col-sm-4 col-xs-12 profile_details text-center"><div class="well profile_view"><div class="col-sm-12"><h4 class="brief"><i>Position: '+ role +'</i></h4><div class="left col-xs-7"><h4><strong>'+ profileObj.first_name + ' ' + profileObj.last_name +'</strong></h4><p><strong>ID Card: </strong> '+ profileObj.id_card +'</p><ul class="list-unstyled"><li><i class="fa fa-user"></i> Username: '+ profileObj.username +' </li><li><i class="fa fa-building"></i> Department: <span title="'+ currRole +'">'+currRoleShr+'</span></li></ul></div><div class="right col-xs-5 text-center"><img src="../../images/img.jpg" alt="" class="img-circle img-responsive"></div></div><div class="col-xs-12 bottom text-center"><div class="col-xs-12 col-sm-6 emphasis"></div><div class="col-xs-12 col-sm-6 emphasis"><a href="/director/lecturer_profile/'+ profileObj.lecturer_id +'" type="button" class="btn btn-primary btn-xs"><i class="fa fa-user"> </i> View Profile</a></div></div></div></div></div>');
          });
        } 
        else if(role == 'Manager')
        {
          $.each(data.managers, (index, profileObj) => {
            let currRole = dpt[profileObj.department_id];
            let currRoleShr = dpt_short[profileObj.department_id];
            $('#items').append('<div class="text_center"><div class="col-md-4 col-sm-4 col-xs-12 profile_details text-center"><div class="well profile_view"><div class="col-sm-12"><h4 class="brief"><i>Position: '+ role +'</i></h4><div class="left col-xs-7"><h4><strong>'+ profileObj.first_name + ' ' + profileObj.last_name +'</strong></h4><p><strong>ID Card: </strong> '+ profileObj.id_card +'</p><ul class="list-unstyled"><li><i class="fa fa-user"></i> Username: '+ profileObj.username +' </li><li><i class="fa fa-building"></i> Department: <span title="'+ currRole +'">'+currRoleShr+'</span></li></ul></div><div class="right col-xs-5 text-center"><img src="../../images/img.jpg" alt="" class="img-circle img-responsive"></div></div><div class="col-xs-12 bottom text-center"><div class="col-xs-12 col-sm-6 emphasis"></div><div class="col-xs-12 col-sm-6 emphasis"><a href="/director/manager_profile/'+ profileObj.username +'" type="button" class="btn btn-primary btn-xs"><i class="fa fa-user"> </i> View Profile</a></div></div></div></div></div>');
          });
        }
        else if(role == 'Student')
        {
          $.each(data.students, (index, profileObj) => {
            let currRole = dpt[profileObj.department_id];
            let currRoleShr = dpt_short[profileObj.department_id];
            
            $('#items').append('<div class="text_center"><div class="col-md-4 col-sm-4 col-xs-12 profile_details text-center"><div class="well profile_view"><div class="col-sm-12"><h4 class="brief"><i>Position: '+ role +'</i></h4><div class="left col-xs-7"><h4><strong>'+ profileObj.first_name + ' ' + profileObj.last_name +'</strong></h4><p><strong>ID Card: </strong> '+ profileObj.id_card +'</p><ul class="list-unstyled"><li><i class="fa fa-user"></i> Username: '+ profileObj.username +' </li><li><i class="fa fa-building"></i> Department: <span title="'+ currRole +'">'+currRoleShr+'</span></li></ul></div><div class="right col-xs-5 text-center"><img src="../../images/img.jpg" alt="" class="img-circle img-responsive"></div></div><div class="col-xs-12 bottom text-center"><div class="col-xs-12 col-sm-6 emphasis"></div><div class="col-xs-12 col-sm-6 emphasis"><a href="/director/student_profile/'+ profileObj.student_id +'" type="button" class="btn btn-primary btn-xs"><i class="fa fa-user"> </i> View Profile</a></div></div></div></div></div>');
          });
        }
      });
    });
    
  });
</script>
@endsection
