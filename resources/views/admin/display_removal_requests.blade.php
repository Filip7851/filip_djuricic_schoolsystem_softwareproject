@extends('layout.core')

@section('title', 'Browse Your Requests')

@section('content')
@if(session()->has('request_removed', ''))
    <script>
        swal(
            'Removed',
            'Successfully Removed Request',
            'success'
        );
    </script>
@endif
    <div class="right_col" role="main">
        <div class="clearfix"></div>
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Manager Student Removal Requests</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    @if(count($demands) <= 0)
                        <strong>Currently, you have no requests for student removal, that are either approved, rejected or pending.</strong><br><br>
                    @else
                    <p>Here, you can view requests for removal of student from the system.</p>
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Request ID</th>
                            <th class="column-title">Manager</th>
                            <th class="column-title">Student</th>
                            <th class="column-title">Request Title</th>
                            <th class="column-title">Status</th>
                            <th class="column-title no-link last"><span class="nobr">Read More</span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($demands as $demand)
                                    @if($demand->demand_id % 2 == 0)
                                        <tr class="even pointer">
                                            <td>{{ $demand->demand_id }}</td>
                                            <td>{{ App\Manager::find($demand->manager_id)->first_name.' '.App\Manager::find($demand->manager_id)->last_name }}</td>
                                            <td>{{ \App\Student::find($demand->student_id)->first_name.' '.\App\Student::find($demand->student_id)->last_name }}</td>
                                            <td>{{ $demand->request_title }}</td>
                                            <td>
                                                @if($demand->approved == 0)
                                                    <strong>UNRESOLVED</strong>
                                                @elseif($demand->approved == -1)
                                                    <strong>REJECTED</strong>
                                                @elseif($demand->approved == 1)
                                                    <strong>ACCEPTED</strong>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('director.requests.read_more', ['request_id' => $demand->demand_id]) }}" class="btn btn-primary">Open</a>
                                            </td>
                                        </tr>
                                    @else
                                        <tr class="odd pointer">
                                            <td>{{ $demand->demand_id }}</td>
                                            <td>{{ App\Manager::find($demand->manager_id)->first_name.' '.App\Manager::find($demand->manager_id)->last_name }}</td>
                                            <td>{{ \App\Student::find($demand->student_id)->first_name.' '.\App\Student::find($demand->student_id)->last_name }}</td>
                                            <td>{{ $demand->request_title }}</td>
                                            <td>
                                                @if($demand->approved == 0)
                                                    <strong>UNRESOLVED</strong>
                                                @elseif($demand->approved == -1)
                                                    <strong>REJECTED</strong>
                                                @elseif($demand->approved == 1)
                                                    <strong>ACCEPTED</strong>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('director.requests.read_more', ['request_id' => $demand->demand_id]) }}" class="btn btn-primary">Open</a>
                                            </td>
                                        </tr>
                                    @endif
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection