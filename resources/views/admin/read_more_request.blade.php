@extends('layout.core')

@section('title', 'Read More About This Request')

@section('content')
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-3 mail_list_column"> 
                                <div class="x_title">
                                  <h5>More From {{ $manager->first_name.' '.$manager->last_name }}</h5>
                                </div>
                                @foreach(\App\Demand::all()->where('approved', 0)->where('demand_id', '!=', $demand->demand_id)->where('manager_id', '=', $demand->manager_id) as $d)
                                    <a href="{{ route('director.requests.read_more', ['request_id' => $d->demand_id]) }}">
                                        <div class="mail_list">
                                            <div class="left">
                                                <i class="fa fa-circle"></i>
                                            </div>
                                            <div class="right">
                                                <h3>{{ $manager->first_name.' '.$manager->last_name }}</h3>
                                                <p>{{ $d->request_body }}</p>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                      <!-- /MAIL LIST -->

                      <!-- CONTENT MAIL -->
                      <div class="col-sm-9 mail_view">
                        <div class="inbox-body">
                          <div class="mail_heading row">
                            <div class="col-md-8">
                                <div class="btn-group">
                                    @if($demand->student_id == null)
                                    <form action="{{ route('director.requests.approve', ['request_id' => $demand->demand_id]) }}" method="POST">
                                        <div class="form-group">
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check"></i> Approve</button>
                                        </div>
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                    </form>
                                    <form action="{{ route('director.requests.reject', ['request_id' => $demand->demand_id]) }}" method="POST">
                                        <div class="form-group">
                                            <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-ban"></i> Reject</button>
                                        </div>
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                    </form>
                                    @else
                                    <form action="{{ route('director.student.approve_removal', ['request_id' => $demand->demand_id]) }}" method="POST">
                                      <div class="form-group">
                                        <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check"></i> Approve</button>
                                      </div>
                                      {{ method_field('PUT') }}
                                      {{ csrf_field() }}
                                    </form>
                                    <form action="{{ route('director.student.reject_removal', ['request_id' => $demand->demand_id]) }}" method="POST">
                                        <div class="form-group">
                                            <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-ban"></i> Reject</button>
                                        </div>
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                    </form>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                <strong><p class="date">Request submitted {{ $demand->created_at->diffForHumans() }}</p></strong>
                            </div>
                            <div class="col-md-12">
                                <h4> {{ $demand->request_title }}</h4>
                            </div>
                          </div>
                          <div class="sender-info">
                            <div class="row">
                              <div class="col-md-12">
                                Request by
                                <strong>{{ $manager->first_name.' '.$manager->last_name }}</strong>
                                <span>({{ $manager->email }})</span>
                              </div>
                            </div>
                          </div>
                          <div class="view-mail">
                            {{ $demand->request_body }}
                          </div>
                        </div>
                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection