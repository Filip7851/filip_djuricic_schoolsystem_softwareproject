@extends('layout.login_core')

@section('title', 'Director Login Page')

@section('login_title', 'Director Login')
@section('action', '/director/login')
@section('login_links')
    <h2><strong><a href="{{ route('manager.show') }}">Manager Login</a></strong></h2>
    <h2><strong><a href="{{ route('lecturer.show') }}">Lecturer Login</a></strong></h2>
    <h2><strong><a href="{{ route('student.show') }}">Student Login</a></strong></h2>
@endsection