@extends('layout.core')

@section('title', 'Director Profile Overview')

@section('content')
@if(session()->has('profile_modified'))
  <script>
    swal(
      'Modified',
      'Successfully Modified Profile',
      'success'
    );
  </script>
@endif
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>User Profile</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>
    
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Profile Overview</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
              <div class="profile_img">
                <div id="crop-avatar">
                  <!-- Current avatar -->
                  <img class="img-responsive avatar-view" src="../../images/picture.jpg" alt="Avatar" title="Change the avatar">
                </div>
              </div>
              <h3>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h3>

              <ul class="list-unstyled user_data">
                <li><i class="fa fa-user user-profile-icon"></i> {{ Auth::user()->username }}
                </li>
                
                <li>
                  <i class="fa fa-envelope user-profile-icon"></i> {{ Auth::user()->email }}
                </li>

                <li>
                  <i class="fa fa-briefcase user-profile-icon"></i> Director
                </li>
                
                <li><i class="fa fa-map-marker user-profile-icon"></i> {{ Auth::user()->address.', '.Auth::user()->city }}
                </li>

                <li>
                  <i class="fa fa-id-card" aria-hidden="true"></i> {{ Auth::user()->id_card }}
                </li>
              </ul>

              <a class="btn btn-success" href="{{ route('director.profile.showEdit') }}"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
              <br>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="profile_title">
                <div class="col-md-6">
                  <h2>User Activity Report</h2>
                </div>
              </div>

              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                    <!-- start recent activity -->
                    <ul class="messages">
                      @foreach($activities as $activity)
                          <li>
                            <img src="../../images/img.jpg" class="avatar" alt="Avatar">
                            <div class="message_wrapper">
                              <h4 class="heading">{{ $activity->director->first_name.' '.$activity->director->last_name }}</h4>
                              <blockquote class="message">{{ $activity->activity_content }}</blockquote>
                              <br />
                              <p class="url">
                                <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                <a href="#"><i class="fa fa-clock-o"></i> {{ $activity->created_at->diffForHumans() }} </a>
                              </p>
                            </div>
                          </li>
                      @endforeach
                    </ul>
                    <!-- end recent activity -->
                    <div class="text-right">
                      {!! $activities->links() !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection