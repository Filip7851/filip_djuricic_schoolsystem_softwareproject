@extends('layout.core')

@section('title', 'Conversation Overview')

@section('content')
@if(session()->has('sent_msg', ''))
    <script>
        swal(
            'Sent',
            'Successfully Sent Message',
            'success'
        );
    </script>
@endif
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-3 mail_list_column"> 
                                <div class="x_title">
                                  <h5>Current Conversation</h5>
                                </div>
                                <a href="{{ route('director.show.conversation', ['conversation' => $conversation]) }}">
                                    <div class="mail_list">
                                        <div class="left">
                                            <i class="fa fa-circle"></i>
                                        </div>
                                        <div class="right">
                                          @if($lastMsgSentByManager !== null)
                                            <h3>{{ $lastMsgSentByManager->manager->first_name.' '.$lastMsgSentByManager->manager->last_name }}</h3>
                                            <p>{{ $lastMsgSentByManager->message_content }}</p>
                                          @else
                                            <h3>{{ Auth::user()->first_name.' '.Auth::user()->last_name }} to {{ App\Manager::find($conversation->messages[0]->manager->id)->first_name.' '.App\Manager::find($conversation->messages[0]->manager->id)->last_name }}</h3>
                                            <p>{{ $conversation->messages[0]->message_content }}</p>
                                          @endif
                                        </div>
                                    </div>
                                </a>
                            </div>
                      <!-- /MAIL LIST -->

                      <!-- CONTENT MAIL -->
                      <div class="col-sm-9 mail_view">
                        <div class="inbox-body">
                          <div class="mail_heading row">
                            <div class="col-md-8">
                                <div class="btn-group">
                                  <a class="btn btn-sm btn-primary" type="button" href="#reply"><i class="fa fa-reply"></i> Reply</a>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                
                            </div>
                            @foreach($conversationMessages as $message)
                                @if($message->sent_by == 0)
                                  <div class="col-md-12">
                                    <h4><a href="{{ route('director.manager.profile.show', ['username' => $message->manager->username]) }}" title="View Profile">{{ $message->manager->first_name.' '.$message->manager->last_name }} to you - {{ $message->created_at->diffForHumans() }}</a></h4>
                                    <h5>{{ $message->message_content }}</h5>
                                  </div>
                                @else
                                  <div class="col-md-12">
                                    <h4>{{ Auth::user()->first_name.' '.Auth::user()->last_name }} - {{ $message->created_at->diffForHumans() }}</h4>
                                    <h5>{{ $message->message_content }}</h5>
                                  </div>
                                @endif
                            @endforeach
                          </div>
                          <div class="sender-info">
                            <div class="row">
                              <div class="col-md-12" id="reply">
                                <hr>
                                <div class="text-right">
                                  {!! $conversationMessages->links() !!}
                                </div>
                                <form action="{{ route('director.message.send', ['conversation' => $conversation]) }}" method="post">
                                    <textarea name="reply_msg" id="reply_msg" class="form-control" placeholder="Reply"></textarea>
                                    @if ($errors->has('reply_msg'))
                                      <strong style="color: red">{{ $errors->first('reply_msg') }}</strong>
                                    @endif
                                    
                                    <hr>
                                    <div class="text-right">
                                      <button class="btn btn-primary" type="submit">Reply</button>
                                    </div>
                                    {{ csrf_field() }}
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection