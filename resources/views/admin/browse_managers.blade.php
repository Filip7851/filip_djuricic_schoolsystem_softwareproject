@extends('layout.core')

@section('title', 'Browse Managers')

@section('content')
@if(session()->has('manager_removed'))
  <script>
    swal(
      'Removed',
      'Successfully Removed Manager',
      'success'
    );
  </script>
@endif
@if(session()->has('manager_inserted'))
  <script>
    swal(
      'Inserted',
      'Successfully Inserted Manager',
      'success'
    );
  </script>
@endif
@if(session()->has('manager_modified'))
  <script>
    swal(
      'Modified',
      'Successfully Modified Manager',
      'success'
    );
  </script>
@endif
<div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Browse Managers</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Here, all the managers are listed for browsing. To search using any criteria, type into search box.
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email</th>
                          <th>ID Card</th>
                          <th>Username</th>
                          <th>Department</th>
                          <th>Modify</th>
                          <th>Remove</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                        @foreach($managers as $manager)
                            <tr>
                                <td>{{ $manager->first_name }}</td>
                                <td>{{ $manager->last_name }}</td>
                                <td>{{ $manager->email }}</td>
                                <td>{{ $manager->id_card }}</td>
                                <td>{{ $manager->username }}</td>
                                <td>{{ $manager->department->department_name }}</td>
                                <td><a href="{{ route('director.manager.show_edit', ['id' => $manager->id]) }}">Edit</a></td>
                                <td><a href="{{ route('director.manager.show_remove', ['id' => $manager->id]) }}">Remove</a></td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              @section('datatables_scripts')
                <script src="../../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
                <script src="../../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
                <script src="../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
                <script src="../../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
                <script src="../../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
                <script src="../../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
                <script src="../../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
                <script src="../../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
                <script src="../../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
                <script src="../../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
                <script src="../../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
                <script src="../../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
                <script src="../../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
              @endsection
@endsection