@extends('layout.core')

@section('title', 'Browse Requests')

@section('content')
@if(session()->has('request_approved'))
  <script>
    swal(
      'Approved',
      'Successfully Approved Request',
      'success'
    );
  </script>
@elseif(session()->has('request_rejected'))
    <script>
        swal(
            'Rejected',
            'Successfully Rejected Request',
            'error'
        );
    </script>
@endif
        <div class="right_col" role="main">
        <div class="clearfix"></div>
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Requests</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    @if(count($demands) == 0)
                        <strong>Currently, there are no pending requests.</strong>
                    @else
                    <p>Here, you can read the requests delivered to you by your managers.</p>

                    <div class="table-responsive">
                      <table id="demand_table" class="table table-striped jambo_table">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Request ID</th>
                            <th class="column-title">Manager</th>
                            <th class="column-title">Department</th>
                            <th class="column-title">Urgent</th>
                            <th class="column-title">Request Title</th>
                            <th class="column-title">Approve</th>
                            <th class="column-title">Reject</th>
                            <th class="column-title no-link last"><span class="nobr">Read More</span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($demands as $demand)
                                @if($demand->approved == 0)
                                    @if($demand->demand_id % 2 == 0)
                                        <tr class="even pointer">
                                            <td>{{ $demand->demand_id }}</td>
                                            <td>{{ App\Manager::find($demand->manager_id)->first_name.' '.App\Manager::find($demand->manager_id)->last_name }}</td>
                                            <td>{{ DB::table('managers')->where('id', $demand->manager_id)->join('departments', 'managers.department_id', '=', 'departments.department_id')->get()->first()->department_name }}</td>
                                            <td>
                                                @if($demand->is_urgent == 0)
                                                    <i class="fa fa-ban fa-lg" aria-hidden="true"></i>
                                                @else
                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                @endif
                                            </td>
                                            <td>{{ $demand->request_title }}</td>
                                            <td>
                                                <form action="{{ route('director.requests.approve', ['request_id' => $demand->demand_id]) }}" method="POST">
                                                    <button class="btn btn-success" type="submit">Approve</button>
                                                    {{ method_field('PUT') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            </td>
                                            <td>
                                                <form action="{{ route('director.requests.reject', ['request_id' => $demand->demand_id]) }}" method="POST">
                                                    <button class="btn btn-danger" type="submit">Reject</button>
                                                    {{ method_field('PUT') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            </td>
                                            <td><a href="{{ route('director.requests.read_more', ['request_id' => $demand->demand_id]) }}" class="btn btn-primary">Open</a></td>
                                        </tr>
                                    @else
                                        <tr class="odd pointer">
                                            <td>{{ $demand->demand_id }}</td>
                                            <td>{{ App\Manager::find($demand->manager_id)->first_name.' '.App\Manager::find($demand->manager_id)->last_name }}</td>
                                            <td>{{ DB::table('managers')->where('id', $demand->manager_id)->join('departments', 'managers.department_id', '=', 'departments.department_id')->get()->first()->department_name }}</td>
                                            <td>
                                                @if($demand->is_urgent == 0)
                                                    <i class="fa fa-ban fa-lg" aria-hidden="true"></i>
                                                @else
                                                    <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                                                @endif
                                            </td>
                                            <td>{{ $demand->request_title }}</td>
                                            <td>
                                                <form action="{{ route('director.requests.approve', ['request_id' => $demand->demand_id]) }}" method="POST">
                                                    <button class="btn btn-success" type="submit">Approve</button>
                                                    {{ method_field('PUT') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            </td>
                                            <td>
                                                <form action="{{ route('director.requests.reject', ['request_id' => $demand->demand_id]) }}" method="POST">
                                                    <button class="btn btn-danger" type="submit">Reject</button>
                                                    {{ method_field('PUT') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            </td>
                                            <td><a href="{{ route('director.requests.read_more', ['request_id' => $demand->demand_id]) }}" class="btn btn-primary">Open</a></td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
