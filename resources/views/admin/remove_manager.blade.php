@extends('layout.core')

@section('title', 'Remove Manager')

@section('content')
<div class="row">
  <div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Remove This Manager</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form id="demo-form2" method="POST" action="{{ route('director.manager.remove', ['id' => $manager->id]) }}" data-parsley-validate class="form-horizontal form-label-left">

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first_name">First Name
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="first_name" name="first_name" class="form-control col-md-7 col-xs-12" value="{{ $manager->first_name }}" disabled>
                @if($errors->has('first_name'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('first_name') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_name">Last Name
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="last_name" name="last_name" class="form-control col-md-7 col-xs-12"  value="{{ $manager->last_name }}" disabled>
                @if($errors->has('last_name'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('last_name') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="email" class="form-control col-md-7 col-xs-12" type="email" name="email" value="{{ $manager->email }}" disabled>
                @if($errors->has('email'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('email') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_card">ID Card</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="id_card" id="id_card" class="form-control col-md-7 col-xs-12" value="{{ $manager->id_card }}" disabled>
                  @if($errors->has('id_card'))
                  <div class="text-left">
                      <strong style="color: red">{{ $errors->first('id_card') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Manager Department
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="department_id" class="form-control" id="departments" disabled>
                    <option value="{{ $manager->department->department_id }}">{{ $manager->department->department_name }}</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="text-right">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="{{ route('director.manager.browse') }}" class="btn btn-success">Go Back</a>
                <button type="submit" class="btn btn-danger">Remove</button>
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
              </div>
              </div>
            </div>
            
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection