@extends ('layout.core')

@section('title', 'Admin Dashboard')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
              <div class="count green" id="tot_cou">{{ $directorsCount + $managersCount + $lecturerCount + $studentsCount }}</div>
              <!--<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>Possible Statistics</i></span>-->
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Total Managers</span>
              <div class="count" id="tot_man">{{ $managersCount }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Lecturers</span>
              <div class="count green" id="tot_lec">{{ $lecturerCount }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Students</span>
              <div class="count" id="tot_stud">{{ $studentsCount }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Male Students</span>
              <div class="count green" id="tot_m_stud">{{ $maleStudentsCount }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Female Students</span>
              <div class="count" id="tot_f_stud">{{ $femaleStudentsCount }}</div>
            </div>
          </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Add new manager</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To add new manager, please click the button bellow</h4>
                
                    <a class="btn btn-block btn-success" href="{{route('director.manager.add')}}">Add New</a>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2>Browse Managers</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To browse all managers, please click the button below</h4>
                    
                    <a class="btn btn-block btn-success" href="{{ route('director.manager.browse') }}">Browse</a>
                </div>
              </div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Manage Notifications</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>To manage notifications, please click the button below</h4>
                    
                    <a class="btn btn-block btn-success" href="{{ route('director.requests.browse') }}">Manage</a>
                </div>
            </div>
          </div>
        </div>
    </div>
@endsection
