@extends('layout.core')

@section('title', 'Email Managers')

@section('content')
    <div class="row">
        <div class="right_col" role="main">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Email Managers</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <form id="demo-form2" action="{{ route('director.manager.send_email') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message_subject">Message Subject</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="message_subject" name="message_subject" class="form-control col-md-7 col-xs-12">
                                    @if($errors->has('message_subject'))
                                        <div class="text-left">
                                            <strong style="color: red">{{ $errors->first('message_subject') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message_content">Message Content</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea id="message_content" class="form-control" name="message_content"></textarea>
                                    @if($errors->has('message_content'))
                                        <div class="text-left">
                                            <strong style="color: red">{{ $errors->first('message_content') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-success" type="submit">Send Email</button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection