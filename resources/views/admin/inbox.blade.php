@extends('layout.core')

@section('title', 'Conversation Overview')

@section('content')
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-3 mail_list_column"> 
                                <a id="compose" class="btn btn-sm btn-success btn-block" href="{{ route('director.conversation.new') }}">NEW CONVERSATION</a>
                                <div class="x_title">
                                  <h5>Your Conversations</h5>
                                </div>
                                @foreach($userConvos as $k => $conversation)
                                    <a href="{{ route('director.show.conversation', ['conversation' => $conversation]) }}">
                                        <div class="mail_list">
                                            <div class="left">
                                                <i class="fa fa-circle"></i>
                                            </div>
                                            <div class="right">
                                                <!-- SHOULD BE OTHER CONV -->
                                                <!-- CONVO JUST STARTED -->
                                                @if(count($userConvos[$k]->messages) == 1)
                                                  @foreach($conversation->messages as $message)
                                                      @if($conversation->started_convo == 0)
                                                        <h3>{{ $message->manager->first_name.' '.$message->manager->last_name }}</h3>
                                                        <p>{{ $message->message_content }}</p>
                                                      @elseif($conversation->started_convo == 1)
                                                        @if($message->sent_by == 0)
                                                          <h3>{{ $message->manager->first_name.' '.$message->manager->last_name }}</h3>
                                                          <p>{{ $message->message_content }}</p>
                                                        @else
                                                          <h3>{{ Auth::user()->first_name.' '.Auth::user()->last_name }} to {{ App\Manager::find($conversation->messages[0]->manager->id)->first_name.' '.App\Manager::find($conversation->messages[0]->manager->id)->last_name }}</h3>
                                                          <p>{{ $message->message_content }}</p>
                                                        @endif
                                                      @endif
                                                  @endforeach
                                                @else
                                                  <!-- CONVO GOING ON -->
                                                  <h3>{{ $conversation->messages[$k]->manager->first_name.' '.$conversation->messages[$k]->manager->last_name }}</h3>
                                                  <p>{{ $conversation->messages[$k]->message_content }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                      <!-- /MAIL LIST -->

                      <!-- CONTENT MAIL -->
                      <div class="col-sm-9 mail_view">
                        <div class="inbox-body">
                          <div class="mail_heading row">
                            <div class="col-md-8">
                                <div class="btn-group">
                                    <!-- BUTTONS -->
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                
                            </div>
                            <div class="col-md-12">
                                <!-- SHOULD BE CONVO TITLE -->
                            </div>
                          </div>
                          <div class="sender-info">
                            <div class="row">
                              <div class="col-md-12">
                                <!-- OTHER PARTICIPANT -->
                                Select conversation in the left menu to view more.
                              </div>
                            </div>
                          </div>
                          <div class="view-mail">
                            <!-- CONTENT OF MSG -->
                          </div>
                        </div>
                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection