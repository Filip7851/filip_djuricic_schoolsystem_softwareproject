@extends('layout.login_core')

@section('title', 'Student Login Page')

@section('login_title', 'Student Login')
@section('action', '/student/login')
@section('login_links')
    <h2><strong><a href="{{ route('director.show') }}">Director Login</a></strong></h2>
    <h2><strong><a href="{{ route('manager.show') }}">Manager Login</a></strong></h2>
    <h2><strong><a href="{{ route('lecturer.show') }}">Lecturer Login</a></strong></h2>
@endsection