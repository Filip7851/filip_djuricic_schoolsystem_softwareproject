<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'conversation_id',
        'director_id',
        'manager_id',
        'message_content',
        'sent_by',
        'started_convo'
    ];
    
    protected $primaryKey = 'message_id';
    
    public function director()
    {
        return $this->hasOne('App\Director', 'id', 'director_id');
    }
    
    public function manager()
    {
        return $this->hasOne('App\Manager', 'id', 'manager_id');
    }
    
    public function conversation()
    {
        return $this->hasOne('App\Conversation', 'conversation_id');
    }
}
