<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Demand;
use App\Manager;
use Illuminate\Support\Facades\Mail;
use App\Mail\RequestUrgent;

class SendUrgentEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $email;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(RequestUrgent $requestUrgent)
    {
        $this->email = $requestUrgent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to('fdjuricic98@gmail.com')->send($this->email);
    }
}
