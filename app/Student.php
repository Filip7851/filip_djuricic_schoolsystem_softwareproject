<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{   
    use Notifiable;
    
    protected $fillable = [
        'department_id',
        'group_id',
        'first_name',
        'last_name',
        'email',
        'username',
        'password',
        'gender',
        'id_card',
    ];
    
    protected $primaryKey = 'student_id';
    protected $guard = 'student';
    
    // hide from JSON
    protected $hidden = [
        'password'
    ];
    
    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }
    
    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }
}
