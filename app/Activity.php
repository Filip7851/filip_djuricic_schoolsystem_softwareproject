<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $primaryKey = 'activity_id';
    
    protected $fillable = [
        'director_id',
        'manager_id',
        'activity_content'
    ];
    
    public function director() 
    {
        return $this->belongsTo('App\Director', 'director_id');    
    }
    
    public function manager()
    {
        return $this->belongsTo('App\Manager', 'manager_id');
    }
}
