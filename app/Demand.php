<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demand extends Model
{
    protected $primaryKey = 'demand_id';
    
    protected $fillable = [
        'manager_id',
        'director_id',
        'is_urgent',
        'request_title',
        'request_body',
        'approved',
        'student_id',
        'lecturer_id',
    ];
    
    public function manager()
    {
        return $this->hasOne('App\Manager', 'id', 'manager_id');
    }
}
