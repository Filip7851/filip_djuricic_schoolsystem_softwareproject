<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Demand;
use App\Manager;

class RequestUrgent extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $demand;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Demand $demand)
    {
        $this->demand = $demand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $manager = Manager::find($this->demand->manager_id);
        
        return $this->markdown('emails.requests.urgent')->with([
            'managerName' => $manager->first_name.' '.$manager->last_name,
            'requestTitle' => $this->demand->request_title    
        ]);
    }
}
