<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Manager;

class DirectorEmailUsers extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $manager;
    public $data;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Manager $manager, $data)
    {
        $this->manager = $manager;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->data['message_subject'])->markdown('emails.director_email_users')->with([
            'managerName' => $this->manager->first_name.' '.$this->manager->last_name,
            'messageContent' => $this->data['message_content']
        ]);
    }
}
