<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Student;

class StudentRemoved extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $student;
    public $manager;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Student $student, \App\Manager $manager)
    {
        $this->student = $student;
        $this->manager = $manager;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Student Removed From System')->markdown('emails.student_removed')->with([
            'studentName' => $this->student->first_name. ' '.$this->student->last_name,
            'managerName' => $this->manager->first_name.' '.$this->manager->last_name
        ]);
    }
}
