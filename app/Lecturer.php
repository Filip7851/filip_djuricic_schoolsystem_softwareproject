<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Lecturer extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
        'department_id',
        'first_name',
        'last_name',
        'email',
        'username',
        'password',
        'gender',
        'id_card',
    ];
    
    protected $hidden = [
        'password'
    ];
    
    protected $primaryKey = 'lecturer_id';
    protected $guard = 'lecturer';
    
    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }
}
