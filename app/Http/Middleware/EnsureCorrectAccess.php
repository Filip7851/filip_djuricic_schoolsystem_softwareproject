<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class EnsureCorrectAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $demand = \App\Demand::find($request->request_id);
        $manager = \App\Manager::find($demand->manager_id);
        
        if($manager->id != Auth::user()->id)
        {
            session()->flash('req_not_users', '');
            
            return redirect()->back();
        }
        
        return $next($request);
    }
}
