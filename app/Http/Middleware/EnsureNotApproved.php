<?php

namespace App\Http\Middleware;

use Closure;

class EnsureNotApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $demand = \App\Demand::find($request->request_id);
        
        if($demand->approved != 0)
        {
            // flash something
            session()->flash('already_solved', '');
            return redirect()->back();
        }
        
        return $next($request);
    }
}
