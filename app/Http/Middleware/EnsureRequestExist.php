<?php

namespace App\Http\Middleware;

use Closure;

class EnsureRequestExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $demand = \App\Demand::find($request->request_id);
        
        if($demand == null)
        {
            session()->flash('no_request', '');
            return redirect()->back();
        }
        
        return $next($request);
    }
}
