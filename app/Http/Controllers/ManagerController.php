<?php
/**
  * This class controls how manager interacts with system.
  *
  * It handles operations such as adding students, groups, browsing groups, making requests, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Director;
use App\Manager;
use App\Lecturer;
use App\Student;
use App\Group;
use App\Demand;
use App\Activity;
use App\Conversation;
use App\Message;

/**
  * This class controls how manager interacts with system.
  *
  * It handles operations such as adding students, groups, browsing groups, making requests, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */
class ManagerController extends Controller
{
    /**
    * Constructor method which specifies that only guards with the auth type manager can access
    * routes controlled by this controller.
    */
    public function __construct()
    {
        $this->middleware('auth:manager');
    }
    
    /**
    * This method displays dashboard for manager.
    * It passes the count of lecturers, groups, students within department to the view for display.
    *
    * @return object
    */
    public function showDashboard()
    {
        // total users in dept
        $totalManagers = 1;
        $totalLecturers = Lecturer::where('department_id', Auth::user()->department_id)->count();
        $totalGroups = Group::where('department_id', Auth::user()->department_id)->count();
        $totalStudents = Student::where('department_id', Auth::user()->department_id)->count();
        $totalMaleStudents = Student::where('department_id', Auth::user()->department_id)->where('gender', 0)->count();
        $totalFemaleStudents = Student::where('department_id', Auth::user()->department_id)->where('gender', 1)->count();
        
        $totalUsers = $totalManagers + $totalLecturers + $totalStudents;
        
        return view('manager.index', compact('totalUsers', 'totalLecturers', 'totalGroups', 'totalStudents', 'totalMaleStudents', 'totalFemaleStudents'));
    }
    
    /**
    * This method shows the form for adding new group to the system (to logged in user department).
    *
    * @return object
    */
    public function showGroupForm()
    {
        return view('manager.add_group');
    }
    
    /**
    * This method processes POST request made through add group form.
    * It first retrieves all data inserted by user through Request object.
    * After that it validates the data, builds group and inserts it into database.
    * It also creates new activity so it is displayed on manager profile.
    *
    * @param $request - request object sent through HttpRequest
    *
    * @return object
    */
    public function addGroup(Request $request)
    {
        // verify input
        $this->validate($request, [
            // class like: 1A
            'class_no' => ['required', 'regex:/^[0-9]{1}[A-Z]+$/']
        ], [
            'class_no.required' => 'Please enter class number (ex: 1B) to proceed',
            'class_no.regex' => 'Class number contains 1 digit followed by 1 capital letter. Example: 1A'
        ]);
        
        // build full class name
        $short = DB::table('departments')->where('department_id', '=', Auth::user()->department_id)->get()->first()->department_short;
        $group_name = $short.'-'.$request->level.'.'.$request->class_no;
        
        // insert into db
        $group = Group::create([
            'department_id' => Auth::user()->department_id,
            'group_name' => strtoupper($group_name),
            'level' => $request->level,
        ]);
        
        session()->flash('group_inserted', '');
        
        Activity::create([
            'manager_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just added new class to '.Auth::user()->department->department_name.' department.'
        ]);
        
        // redirect to the browse class
        return redirect()->route('manager.classes.browse');
    }
    
    /**
    * This method shows the form for adding new student to the system (to logged in user department).
    *
    * @return object
    */
    public function showStudentForm()
    {   
        return view('manager.add_student');
    }
    
    /**
    * This method processes POST request made through add student form.
    * It first retrieves all data inserted by user through Request object.
    * After that it validates the data, builds username and password and inserts it into database.
    * It also creates new activity so it is displayed on manager profile.
    *
    * @param $request - request object sent through HttpRequest
    *
    * @return object
    */
    public function addStudent(Request $request) {
        $this->validate($request, [
            'first_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'last_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'email' => 'required|unique:students',
            'id_card' => [
                            'required',
                            'unique:students',
                            'size:8',
                            'regex:/^[0-9]{7}[a-zA-Z]{1}$/'
            ],
            'gender' => 'required'
        ], [
            'first_name.required' => 'Please first name to proceed.',
            'first_name.regex' => 'First name must contain characters only.',
            'last_name.required' => 'Please last name to proceed.',
            'last_name.regex' => 'Last name must contain characters only.',
            'email.required' => 'Please enter email to proceed.',
            'email.unique' => 'We already have that email.',
            'id_card.required' => 'Please enter ID Card to proceed.',
            'id_card.unique' => 'We already have that ID Card.',
            'id_card.size' => 'ID Card Contains 8 Characters',
            'id_card.regex' => 'ID format is 7 numbers followed by 1 letter. Example: 0123456A',
            'gender.required' => 'Please choose student gender to proceed.'
        ]);
        
        $username = strtolower($request->first_name).strtolower($request->last_name[0]);
        $password = 'student'.mb_substr($request->id_card, 0, 7);
        
        $student = Student::create([
            'department_id' => Auth::user()->department_id,
            'group_id' => $request->group,
            'first_name' => ucfirst($request->first_name),
            'last_name' => ucfirst($request->last_name),
            'email' => strtolower($request->email),
            'username' => $username,
            'password' => Hash::make($password),
            'gender' => $request->gender,
            'id_card' => $request->id_card
        ]);
        
        session()->flash('student_inserted', '');
        
        Activity::create([
            'manager_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just added new student to '.Auth::user()->department->department_name.' department. This student was added to '.$student->group->group_name.' group.'
        ]);
        
        return redirect()->route('manager.classes.browse');
    }
    
    /**
    * This method shows the form for adding new lecturer to the system (to logged in user department).
    *
    * @return object
    */
    public function showLecturerForm()
    {
        return view('manager.add_lecturer');   
    }
    
    /**
    * This method processes POST request made through add lecturer form.
    * It first retrieves all data inserted by user through Request object.
    * After that it validates the data, builds username and password and inserts it into database.
    * It also creates new activity so it is displayed on manager profile.
    *
    * @param $request - request object sent through HttpRequest
    *
    * @return object
    */
    public function addLecturer(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:students',
            'id_card' => [
                            'required',
                            'unique:students',
                            'size:8',
                            'regex:/^[0-9]{7}[a-zA-Z]{1}$/'
            ]
        ], [
            'first_name.required' => 'Please enter first name to proceed.',
            'last_name.required' => 'Please enter last name to proceed.',
            'email.required' => 'Please enter email to proceed.',
            'email.unique' => 'We already have that email.',
            'id_card.required' => 'Please enter ID Card to proceed.',
            'id_card.unique' => 'We already have that ID Card.',
            'id_card.size' => 'ID Card Contains 8 Characters',
            'id_card.regex' => 'ID format is 7 numbers followed by 1 letter. Example: 0123456A',
        ]);
        
        $username = strtolower($request->first_name).strtolower($request->last_name[0]);
        $password = 'lecturer'.mb_substr($request->id_card, 0, 7);
        
        $student = Lecturer::create([
            'department_id' => Auth::user()->department_id,
            'first_name' => ucfirst($request->first_name),
            'last_name' => ucfirst($request->last_name),
            'email' => strtolower($request->email),
            'username' => $username,
            'password' => Hash::make($password),
            'gender' => $request->gender,
            'id_card' => $request->id_card
        ]);
        
        session()->flash('lecturer_inserted', '');
        
        Activity::create([
            'manager_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just added new lecturer to '.Auth::user()->department->department_name.' department.'
        ]);
        
        return redirect()->route('manager.lecturer.browse');
    }
    
    /**
    * This method shows the form for making request to the director.
    *
    * @return object
    */
    public function showRequestForm()
    {
        return view('manager.make_request');    
    }
    
    /**
    * This method processes POST request made through make request form.
    * It first retrieves all data inserted by user through Request object.
    * After that it validates the data, it inserts new demand (request) to the database.
    * It also creates new activity so it is displayed on manager profile.
    *
    * @param $request - request object sent through HttpRequest
    *
    * @return object
    */
    public function makeRequest(Request $request)
    {
        $this->validate($request, [
            'request_title' => ['required', 'max:50'],
            'request_body' => 'required'    
        ], [
            'request_title.required' => 'Please enter title of the request to proceed.',
            'request_title.size' => 'Request title cannot have more than 50 characters.',
            'request_body.required' => 'Please enter body of the request to proceed.'
        ]);
        
        $director = Director::find(1);
        
        // make request here
        $demand = Demand::create([
            'manager_id' => Auth::user()->id,
            'director_id' => 1,
            'is_urgent' => ($request->is_urgent == 1) ? 1 : 0,
            'request_title' => $request->request_title,
            'request_body' => $request->request_body,
            'approved' => 0,
        ]);
        
        if($request->is_urgent == 1)
        {
            // send email
            Mail::to($director->email)->send(new \App\Mail\RequestUrgent($demand));
        }
        
        session()->flash('request_made', '');
        
        Activity::create([
            'manager_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just submitted request to director.'
        ]);
        
        // post
        return redirect()->route('manager.requests.browse');
    }
    
    /**
    * This method shows the requests made by the logged in manager.
    *
    * @return object
    */
    public function displayRequests()
    {
        // display only this manager's requests
        $demands = Demand::where('manager_id', Auth::user()->id)->where('request_title', '!=', 'Student Removal')->get();
        
        return view('manager.display_requests', compact('demands'));
    }
    
    /**
    * This method allows the manager to 'read more' about his request.
    *
    * @param $request_id - request_id received through HttpRequest
    *
    * @return object
    */
    public function readMoreRequest($request_id)
    {
        $demand = Demand::find($request_id);
        
        $status = '';
        
        if($demand->approved == 0)
        {
            $status = 'UNRESOLVED';
        }
        elseif($demand->approved == -1)
        {
            $status = 'REJECTED';
        }
        elseif($demand->approved == 1)
        {
            $status = 'APPROVED';
        }
        
        return view('manager.read_more_request', compact('demand', 'status'));
    }
    
    /**
    * This method makes DELETE request and removes the selected request from demand list.
    *
    * @param $request_id - request_id received through HttpRequest
    *
    * @return object
    */
    public function deleteRequest($request_id)
    {
        $demand = Demand::find($request_id);
        
        $demand->delete();
        
        session()->flash('request_removed', '');
        
        return redirect()->route('manager.requests.browse');
    }
    
    /**
    * This method shows the groups (within manager's department) for browsing.
    *
    * @return object
    */
    public function displayClassForm()
    {
        // receive class by dept
        $allGroups = Group::where('department_id', Auth::user()->department_id)->where('level', 4)->get();
        $firstGroup = Group::where('department_id', Auth::user()->department_id)->first();
        $classStudents = Student::where('group_id', $firstGroup->group_id)->get();
        
        return view('manager.browse_classes', compact('allGroups', 'classStudents'));
    }
    
    /**
    * This method is AJAX helper method which sends back JSON object (with retrieved groups) to view for display.
    *
    * @param $id - id received through HttpRequest
    *
    * @return object
    */
    public function retrieveClasses($id)
    {
        $groups = Group::where('level', $id)->where('department_id', Auth::user()->department_id)->get();
        
        return response()->json($groups);
    }
    
    /**
    * This method is AJAX helper method which sends back JSON object (with retrieved students) to view for display.
    *
    * @param $id - id received through HttpRequest
    *
    * @return object
    */
    public function retrieveStudents($id)
    {
        $students = Student::where('group_id', $id)->get();
        
        return response()->json($students);
    }
    
    /**
    * This method shows the lecturers (within manager's department) for browsing.
    *
    * @return object
    */
    public function displayLecturers()
    {
        $lecturers = Lecturer::where('department_id', Auth::user()->department_id)->get();
        
        return view('manager.browse_lecturers', compact('lecturers'));
    }
    
    /**
    * This method shows the student removal form which allows manager to make request for removing student.
    *
    * @param $student_id - student_id received through HttpRequest
    *
    * @return object
    */
    public function displayStudentRemovalForm($student_id)
    {
        return view('manager.remove_student', compact('student_id'));
    }
    
    /**
    * This method makes POST request, receives data inserted by user, validates it and creates new demand for review by director.
    *
    * @param $student_id - student_id received through HttpRequest
    * @param $request - request object retrieved through HttpRequest
    *
    * @return object
    */
    public function makeStudentRemoveRequest($student_id, Request $request)
    {
        $this->validate($request, [
           'reason' => 'required' 
        ], [
            'reason.required' => 'Please enter reason for removal to continue.'
        ]);
        
        // write the request to the director
        $demand = Demand::create([
            'manager_id' => Auth::user()->id,
            'director_id' => 1,
            'is_urgent' => 1,
            'request_title' => 'Student Removal',
            'request_body' => 'Manager requests that the '.Student::find($student_id)->first_name.' '.Student::find($student_id)->last_name.' be removed from the system. Reason:'.$request->reason,
            'approved' => 0,
            'student_id' => $student_id
        ]);
        
        // send email as urgent to director
        Mail::to(Director::find(1)->email)->send(new \App\Mail\RequestUrgent($demand));
        
        session()->flash('request_made', '');
        
        Activity::create([
            'manager_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just made student removal request.'
        ]);
        
        return redirect()->route('manager.student.removal_requests');
    }
    
    /**
    * This method displays student removal requests made by the logged in manager.
    *
    * @return object
    */
    public function displayStudentRemovalRequests()
    {
        $demands = Demand::where('manager_id', Auth::user()->id)->where('request_title', 'Student Removal')->get();
        
        return view('manager.display_removal_requests', compact('demands'));
    }
    
    /**
    * This method makes PUT (update) request, allowing the manager to confirm student removal (after director previously approved).
    * It removes student and demand from the database.
    *
    * @param $request_id - request_id retrieved through HttpRequest
    *
    * @return object
    */
    public function confirmStudentRemoval($request_id)
    {
        $demand = Demand::find($request_id);
        $student = Student::find($demand->student_id);
        //$manager = Manager::find($demand->manager_id);
        
        // send email to student
        // QUEUE failing.... FIX ?!
        //Mail::to($student->email)->send(new \App\Mail\StudentRemoved($student, $manager));
        
        // remove student
        $student->delete();
        
        // remove demand
        $demand->delete();
        
        session()->flash('confirmed_removal', '');
        
        return redirect()->route('manager.student.removal_requests');
    }
    
    /**
    * This method makes PUT (update) request, allowing the manager to dispose student removal (after director previously rejected it).
    * It removes demand from the database and keeps student in.
    *
    * @param $request_id - request_id retrieved through HttpRequest
    *
    * @return object
    */
    public function disposeStudentRemoval($request_id)
    {
        $demand = Demand::find($request_id);
        
        // remove demand
        $demand->delete();
        
        session()->flash('disposed_removal', '');
        
        return redirect()->route('manager.student.removal_requests');
    }
    
    /**
    * This method allows manager to have overview of his profile as well as his activities.
    *
    * @param $username - username retrieved through HttpRequest
    *
    * @return object
    */
    public function showProfile($username)
    {
        $manager = Manager::whereUsername($username)->first();
        
        // retrieve user activities
        $activities = Activity::where('manager_id', $manager->id)->paginate(5);
        
        $displayEdit = 0;
        
        $man_dept = $manager->department->department_name;
        
        return view('manager.profile_view', compact('manager', 'displayEdit', 'man_dept', 'activities'));
    }
    
    /**
    * This method displays form for manager editing profile.
    * It retrieves manager and sends it to the admin.profile_edit for display.
    *
    * @return object
    */
    public function showEditProfile()
    {
        $manager = Manager::find(Auth::user()->id);
        
        return view('manager.profile_edit', compact('manager'));
    }
    
    /**
    * This method processes PUT (update) request which first retrieves manager that needs to be updated, validates form and
    * and updates manager in the database.
    * It also creates activity which is displayed on manager profile.
    *
    * @param $request - request object received through HttpRequest
    *
    * @return object
    */
    public function editProfile(Request $request)
    {
        $manager = Manager::find(Auth::user()->id);
        
        // messages
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'email' => 'required',
            // 0124801A
            // try to implement regex?
            'id_card' => [
                            'required',
                            'size:8',
                            'regex:/^[0-9]{7}[a-zA-Z]{1}$/'
            ],
        ], [
            'first_name.required' => 'Please Enter First Name',
            'last_name.required' => 'Please Enter Last Name',
            'username.required' => 'Please Enter Username',
            'email.required' => 'Please Enter Email',
            'id_card.required' => 'Please Enter ID Card',
            'id_card.size' => 'ID Card Contains 8 Characters',
            'id_card.regex' => 'ID format is 7 numbers followed by 1 letter. Example: 0123456A',
        ]);
        
        $manager->first_name = $request->first_name;
        $manager->last_name = $request->last_name;
        $manager->username = $request->username;
        $manager->email = $request->email;
        $manager->id_card = $request->id_card; 
        
        // edit director
        $manager->save();
        
        session()->flash('profile_modified', '');
        
        Activity::create([
            'manager_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just modified profile.'
        ]);
        
        return redirect()->route('manager.profile.show', ['username' => $manager->username]);
    }

    /**
     * This method displays the selected conversation together with messages and pagination if applicable (15 messages are shown per page).
     *
     * @return object
     */
    public function showConversations()
    {
        // manager can have only 1 conversation
        $message = Auth::user()->messages->first();
        
        $conversation = null;
        $messages = null;
        $lastMsgSentByDirector = null;
        
        if(!count($message) == 0)
        {
            $conversation = Conversation::find($message->conversation_id);
            
            // 15 messages per page
            $messages = Auth::user()->messages()->paginate(15);
            // retrieve last message by director
            $lastMsgSentByDirector = Message::where('conversation_id', $conversation->conversation_id)->where('sent_by', 1)->orderBy('created_at', 'desc')->first();
        }
        
        return view('manager.inbox', compact('conversation', 'messages', 'lastMsgSentByDirector'));
    }

    /**
     * This method is used to process POST request, validate the text and send message to the other participant.
     *
     * @param $conversation - conversation object retrieved through HttpRequest
     * @param $request - request object retrieved through HttpRequest
     *
     * @return object
     */
    public function sendMessage(Conversation $conversation, Request $request)
    {
        $this->validate($request, [
            'reply_msg' => 'required' 
        ], [
            'reply_msg.required' => 'Please enter the message.'
        ]);
        
        $director = Director::find(1);
        
        // store msg's sent by manager
        Message::create([
            'conversation_id' => $conversation->conversation_id,
            'director_id' => 1,
            'manager_id' => Auth::user()->id,
            'message_content' => $request->reply_msg,
            // sent by manager
            'sent_by' => 0    
        ]);
        
        // create new activity
        Activity::create([
            'manager_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just sent new message to '.$director->first_name.' '.$director->last_name.'.'
        ]);
         
        return redirect()->back();
    }
}
