<?php
/**
  * This class controls how student interacts with system.
  *
  * It handles operations such as displaying dashboard, profile, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Student;

/**
  * This class controls how student interacts with system.
  *
  * It handles operations such as displaying dashboard, profile, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */
class StudentController extends Controller
{
    /**
    * Constructor method which specifies that only guards with the auth type student can access
    * routes controlled by this controller.
    */
    public function __construct()
    {
        $this->middleware('auth:student');   
    }
    
    /**
    * This method displays dashboard for student.
    *
    * @return object
    */
    public function showDashboard()
    {
        return view('student.index');
    }
    
    /**
    * This method displays profile overview for lecturer.
    *
    * @param $student - student object received through HttpRequest
    *
    * @return object
    */
    public function showProfile(Student $student)
    {
        return view('student.profile_view', compact('student', 'displayEdit'));
    }
}
