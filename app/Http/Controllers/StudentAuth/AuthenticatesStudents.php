<?php
/**
  * This class controls how student is authenticated.
  *
  * It handles operations such as validating form to assure that all passed data is correct and to 
  * also validates that the username / password are correct.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */

namespace App\Http\Controllers\StudentAuth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

/**
  * This class controls how student is authenticated.
  *
  * It handles operations such as validating form to assure that all passed data is correct and to 
  * also validates that the username / password are correct.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */
class AuthenticatesStudents extends Controller
{   
    /**
      * Where to redirect in case of unexpected error.
    */
    protected $redirectTo = '/';
    
    /**
      * This is the constructor of the class.
      * It sets the middleware so that lecturer, manager, directors, students can't access this login form
      * if they are already authenticated.
    */
    public function __construct() {
        // guests
        $this->middleware(['guest:lecturer', 'guest:manager', 'guest:director', 'guest:student'], ['except' => 'logout']);
    }
    
    /**
      * This method renders the view where users can login
      *
      * @return object
    */
    public function showLoginPage()
    {
        return view('student.login');
    }
    
    /**
      * This method validates the login (through helper method) and attemps to login the user with credentials.
      * If the user is logged in we redirect him to the dashboard otherwise we send failed login response.$_COOKIE
      *
      * @param $request - request object sent through HttpRequest
      *
      * @return object
    */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        
        // successfull login
        if($this->attemptLogin($request))
        {
            // flash session
            session()->flash('logged_in', 'Successfully logged in.');
            
            // redirect to dashboard
            return redirect()->intended(route('student.dashboard'));
        }
        
        // failed login
        return $this->sendFailedLoginResponse($request);
    }
    
    /**
      * This method logs the user out and redirects him to the login page.
      *
      * @param $request - request object sent through HttpRequest
      *
      * @return object
    */
    public function logout(Request $request) {
        $this->guard()->logout();
        
        $request->session()->flush();
        
        $request->session()->regenerate();
        
        return redirect()->intended(route('student.login'));
    }
    
    /**
      * This method validates the login credentials provided by user.
      *
      * @param $request - request object sent through HttpRequest
      *
      * @return void
    */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ], $this->messages());
    }
    
    /**
      * This method stores the error messages that are displayed to the user.
      *
      * @return array
    */
    protected function messages() {
        return [
            'username.required' => 'Please enter your username to proceed.',
            'password.required' => 'Please enter your password to proceed.'
        ];
    }
    
     /**
      * This method attempts to log the user in by checking credentials.
      *
      * @param $request - request object sent through HttpRequest
      *
      * @return object
    */
    protected function attemptLogin(Request $request) {
        return $this->guard()->attempt($this->credentials($request), true);
    }
    
    /**
      * This method defines which fields (by name) are going to be validated.
      *
      * @param $request - request object sent through HttpRequest
      *
      * @return object
    */
    protected function credentials(Request $request)
    {
        return $request->only('username', 'password');
    }
    
    /**
      * This method returns the failed login response with error messages.
      *
      * @param $request - request object sent through HttpRequest
      *
      * @return object
    */
    protected function sendFailedLoginResponse(Request $request) {
        $errors = ['fail' => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        
        return redirect()->route('student.show')
            ->withInput($request->only('username'))
            ->withErrors($errors);
    }
    
    /**
      * This method defines which authentication guard is going to be used when the user is authenticated.
      *
      * @return object
    */
    protected function guard()
    {
        return Auth::guard('student');
    }
}