<?php
/**
  * This class controls how lecturer interacts with system.
  *
  * It handles operations such as displaying dashboard, profile, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Lecturer;

/**
  * This class controls how lecturer interacts with system.
  *
  * It handles operations such as displaying dashboard, profile, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */
class LecturerController extends Controller
{
    /**
    * Constructor method which specifies that only guards with the auth type lecturer can access
    * routes controlled by this controller.
    */
    public function __construct()
    {
        $this->middleware('auth:lecturer'); 
    }
    
    /**
    * This method displays dashboard for lecturer.
    *
    * @return object
    */
    public function showDashboard()
    {
        return view('lecturer.index');
    }
    
    /**
    * This method displays profile overview for lecturer.
    *
    * @param $lecturer - lecturer object received through HttpRequest
    *
    * @return object
    */
    public function showProfile(Lecturer $lecturer)
    {
        return view('lecturer.profile_view', compact('lecturer', 'displayEdit'));
    }
}
