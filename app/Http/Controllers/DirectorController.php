<?php
/**
  * This class controls how director interacts with system.
  *
  * It handles operations such as adding managers, modifying, removing, retrieving profiles, making AJAX calls, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  * @filesource
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\Director;
use App\Manager;
use App\Lecturer;
use App\Student;
use App\Department;
use App\Demand;
use App\Activity;
use App\Message;
use App\Conversation;

/**
  * This class controls how director interacts with system.
  *
  * It handles operations such as adding managers, modifying, removing, retrieving profiles, making AJAX calls, etc.
  *
  * @author Filip Djuricic - Filip.Djuricic.b31245@mcast.edu.mt
  */
class DirectorController extends Controller
{
    /**
    * Constructor method which specifies that only guards with the auth type director can access
    * routes controlled by this controller.
    */
    public function __construct() {
        $this->middleware('auth:director');
    }
    
    /**
    * This method displays dashboard for director.
    * It passes the count of directors, managers, lecturers, students for view.
    *
    * @return object
    */
    public function showDashboard()
    {
        // retrieve count of directors
        $directorsCount = Director::count();
        
        // retrieve count of managers
        $managersCount = Manager::count();
        
        // retrieve count of lecturers
        $lecturerCount = Lecturer::count();
        
        // retrieve count of students
        $studentsCount = Student::count();
        
        // retrieve count of male students
        $maleStudentsCount = Student::where('gender', 0)->count();
        
        // retrieve count of female students
        $femaleStudentsCount = Student::where('gender', 1)->count();
        
        return view('admin.index', compact('directorsCount', 'managersCount', 'lecturerCount', 'studentsCount', 'maleStudentsCount', 'femaleStudentsCount'));
    }
    
    /**
    * This method shows the form for adding the manager.
    * It first retrieves the departments which do not have manager, and passes it to the admin.add_manager view.
    *
    * @return object
    */
    public function showManagerForm() 
    {
        // already contained managers
        $departments = Department::where('contained', 0)->get();
        
        return view('admin.add_manager', compact('departments'));
    }
    
    /**
    * This method processes POST request made through add manager form.
    * It first retrieves all data inserted by user through Request object.
    * After that it validates the data, builds username, password and creates new manager into database
    * It also creates new activity so it is displayed on director profile.
    *
    * @param $request - request object sent through HttpRequest
    *
    * @return object
    */
    public function addManager(Request $request)
    {   
        // messages
        $this->validate($request, [
            'first_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'last_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'email' => 'required|unique:managers',
            'id_card' => ['required', 'unique:managers', 'size:8', 'regex:/^[0-9]{7}[a-zA-Z]{1}$/']
        ], [
            'first_name.required' => 'Please Enter Manager\'s First Name',
            'first_name.regex' => 'First Name Must Contain Characters Only',
            'last_name.required' => 'Please Enter Manager\'s Last Name',
            'last_name.regex' => 'Last Name Must Contain Characters Only',
            'email.required' => 'Please Enter Manager\'s Email',
            'id_card.required' => 'Please Enter ID Card',
            'id_card.size' => 'ID Card Contains 8 Characters',
            'id_card.unique' => 'That ID card is already used',
            'id_card.regex' => 'ID format is 7 numbers followed by 1 letter. Example: 0123456A',
            'email.unique' => 'We already have that email',
        ]);
        
        // build username
        $username = strtolower($request->first_name).strtolower($request->last_name[0]);
        
        // build password
        $password = 'school'.mb_substr($request->id_card, 0, 7);
        
        $manager = Manager::create([
            'department_id' => $request->department_id,
            'first_name' => ucfirst($request->first_name),
            'last_name' => ucfirst($request->last_name),
            'email' => $request->email,
            'id_card' => $request->id_card,
            'username' => $username,
            'password' => Hash::make($password),
            'in_convo' => 0
        ]);
        
        // modify new dept contained
        $department = Department::find($request->department_id);
        $department->contained = 1;
        
        $department->save();
        
        session()->flash('manager_inserted', '');
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just added new manager to the '.$manager->department->department_name.' department.'
        ]);
        
        return redirect()->route('director.manager.browse');
    }
    
    /**
    * This method retrieves all managers and sends it to the admin.browse_managers view so the admin can easily
    * browse manager within the view.
    * 
    * @return object
    */
    public function showManagerTable() 
    {
        $managers = Manager::all();
           
        return view('admin.browse_managers', compact('managers'));    
    }
    
    /**
    * This method displays the form which allows the director to edit manager profile.
    * It retrieves manager (so the manager data is displayed in the fields), as well as departments, which are not filled
    * so that if director wants to change department he can only do so in departments without manager.
    *
    * @param $id - User ID received through HttpRequest
    *
    * @return object
    */
    public function showModifyForm($id) 
    {
        $manager = Manager::find($id);
        $departments = DB::table('departments')->where('department_id', $manager->department_id)->orWhere(function($query) {
            $query->where('contained', 0);
        })->get(); // Department::where('department_id', $manager->department_id)->where('contained', 0)->get();   
        
        return view('admin.modify_manager', compact('manager', 'departments'));
    }
    
    
    /**
    * This method processes PUT (update) request, retrieves manager that needs to be edited and retrieves data inserted through
    * Request object.
    * It also retrieves manager's current department, validates the input and determines whether department needs to be updated
    * or only other data (such as first name and last name) have been changed.
    * After that, it updates manager and department if needed.
    *
    * @param $id - id received through HttpRequest
    * @param $request - request object sent through HttpRequest
    *
    * @return object
    */
    public function editManager($id, Request $request)
    {
        $manager = Manager::find($id);
        $currentDept = Department::find($manager->department_id);
        
        // messages
        $this->validate($request, [
            'first_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'last_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'email' => 'required',
            'id_card' => ['required', 'size:8', 'regex:/^[0-9]{7}[a-zA-Z]{1}$/']
        ], [
            'first_name.required' => 'Please Enter Manager\'s First Name',
            'first_name.regex' => 'First name must contain characters only.',
            'last_name.required' => 'Please Enter Manager\'s Last Name',
            'last_name.regex' => 'Last name must contain characters only.',
            'email.required' => 'Please Enter Manager\'s Email',
            'email.unique' => 'We already have that email',
            'id_card.required' => 'Please Enter ID Card',
            'id_card.size' => 'ID Card Contains 8 Characters',
            'id_card.regex' => 'ID format is 7 numbers followed by 1 letter. Example: 0123456A',
        ]);
        
        $manager->first_name = $request->first_name;
        $manager->last_name = $request->last_name;
        $manager->email = $request->email;
        $manager->id_card = $request->id_card;
        // build username
        $username = strtolower($request->first_name).strtolower($request->last_name[0]);   
        $manager->username = $username;
        $password = 'school'.mb_substr($request->id_card, 0, 7);
        $manager->password = Hash::make($password);
        $manager->department_id = $request->department_id;
        
        // check if needs to modify dept contained's
        if($currentDept->department_id != $request->department_id)
        {
            // edit contained's
            $currentDept->contained = 0;
            
            $newDept = Department::find($request->department_id);
            $newDept->contained = 1;
            
            $currentDept->save();
            $newDept->save();
            
            Activity::create([
                'director_id' => Auth::user()->id,
                'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just transfered '.$manager->first_name.' '.$manager->last_name.' from '.$currentDept->department_name.' department to '.$newDept->department_name.' department.'
            ]);
        }
        else
        {
            Activity::create([
                'director_id' => Auth::user()->id,
                'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just modfied '.$manager->first_name.' '.$manager->last_name.' manager profile data.'
            ]);
        }
        
        // edit manager
        $manager->save();
        
        session()->flash('manager_modified', '');
        
        return redirect()->route('director.manager.browse');
    }
    
    /**
    * This method displays view for removal of the manager. 
    * It first retrieves manager and pass it to the admin.remove_manager view, where manager data is displayed,
    * and director is asked to confirm that he wants to remove manager.
    *
    * @param $id - id received through HttpRequest
    *
    * @return object
    */
    public function showRemoveForm($id) 
    {
        $manager = Manager::find($id);
        
        return view('admin.remove_manager', compact('manager'));
    }
    
    /**
    * This method processes DELETE request if the director decides to remove manager.
    * It first retrieves the manager that needs to be deleted, deletes him and creates new activity which is displayed on
    * director's profile.
    * It also deletes all activities / demands / messages of the deleted manager to keep application performance up and running.
    *
    * @param $id - id received through HttpRequest
    *
    * @return object
    */
    public function removeManager($id)
    {
        $manager = Manager::find($id);
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just removed '.$manager->first_name.' '.$manager->last_name.' as a manager for '.$manager->department->department_name.' department.'
        ]);
        
        // update department contained
        $manager->department->contained = 0;
        $manager->department->save();
        
        // remove all manager's demands
        foreach($manager->demands as $demand)
        {
            $demand->delete();
        }
        
        // remove all manager's activities
        foreach($manager->activities as $activity)
        {
            $activity->delete();
        }
        
        // remove all manager messages & conversations
        $convo = Conversation::find(Message::where('manager_id', $manager->id)->first()->conversation_id);
        $convo->delete();
        
        foreach($manager->messages as $message)
        {
            $message->delete();
        }
        
        // remove manager from DB
        $manager->delete();
        
        // flash the message
        session()->flash('manager_removed', '');
        
        // return to browse
        return redirect()->route('director.manager.browse');
    }
    
    /**
    * This method first retrieves demands and passes them to the admin.display_requests view for display. 
    * It only displays "normal" requests (ones that are not solved and that are not student removal requests).
    *
    * @return object
    */
    public function displayRequests()
    {
        $demands = Demand::where('approved', '!=', '-1')->where('approved', '!=', '1')->where('student_id', null)->where('lecturer_id', null)->get();
        
        return view('admin.display_requests', compact('demands'));
    }
    
    /**
    * This method processes PUT (update) request which approves the manager's request.
    * It first retrieves the demand which needs to be approved, updates it and creates activity which is displayed on profile.
    *
    * @param $request - request object received through HttpRequest
    *
    * @return object
    */
    public function approveRequest(Request $request)
    {
        $demand = Demand::find($request->request_id);
        
        $demand->approved = 1;
        $demand->save();
        
        session()->flash('request_approved', '');
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just approved '.$demand->manager->first_name.' '.$demand->manager->last_name.'\'s request titled as: '.$demand->request_title.'.'
        ]);
        
        return redirect()->route('director.requests.browse');
    }
    
    /**
    * This method processes PUT (update) request which rejects the manager's request.
    * It first retrieves the demand which needs to be rejected, updates it and creates activity which is displayed on profile.
    *
    * @param $request - request object received through HttpRequest
    *
    * @return object
    */
    public function rejectRequest(Request $request)
    {
        $demand = Demand::find($request->request_id);
        
        $demand->approved = -1;
        $demand->save();
        
        session()->flash('request_rejected', '');
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just rejected '.$demand->manager->first_name.' '.$demand->manager->last_name.'\'s request titled as: '.$demand->request_title.'.'
        ]);
        
        return redirect()->route('director.requests.browse');
    }
    
    /**
    * This method displays read more about request view which allows director to read body of the request.
    * It first retrieves the demand which needs to be displayed and passes it to the admin.read_more_request for display.
    * It also retrieves manager that made request so it is also displayed in admin.read_more_request view.
    *
    * @param $request_id -> request_id received throught HttpRequest
    *
    * @return object
    */
    public function readMoreRequest($request_id)
    {
        $demand = Demand::find($request_id);
        $manager = Manager::find($demand->manager_id);
        
        return view('admin.read_more_request', compact('demand', 'manager'));
    }
    
    /**
    * This method displays the admin.email_managers view which allows manager to email all managers.
    *
    * @return object
    */
    public function showEmailManagerForm()
    {   
        return view('admin.email_managers');
    }
    
    /**
    * This method processes POST request which validates input by director and sends email to all managers. 
    * It also creates activity which is displayed on the director's profile.
    *
    * @param $request - request object received through HttpRequest
    *
    * @return object
    */
    public function sendEmailManagers(Request $request)
    {
        $this->validate($request, [
            'message_subject' => 'required|max:50',
            'message_content' => 'required'
        ], [
            'message_subject.required' => 'Please enter message subject to proceed.',
            'message_subject.max' => 'Message subject must not contain more than 50 characters',
            'message_content.required' => 'Please enter content of the message to proceed.'
        ]);
        
        // retrieve managers
        $managers = Manager::all();
        
        foreach($managers as $manager)
        {   
            // send email to each manager
            Mail::to($manager->email)->send(new \App\Mail\DirectorEmailUsers($manager, ['message_subject' => $request->message_subject, 'message_content' => $request->message_content]));   
        }
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just sent email to all managers.'
        ]);
        
        return redirect()->back();
    }
    
    /**
    * This method first retrieves student removal demands and passes them to the admin.display_removal_requests view for display. 
    * It only displays student removal requests (ones that are not solved and that are not normal requests).
    *
    * @return object
    */
    public function displayStudentRemovalRequests()
    {
        $demands = Demand::where('approved', '!=', '-1')->where('approved', '!=', '1')->where('student_id', '!=', null)->get();

        return view('admin.display_removal_requests', compact('demands'));
    }
    
    /**
    * This method processes PUT (update) request, which first retrieves student removal request which needs to be approved,
    * updates it, and creates activity which is displayed on director profile.
    *
    * @param $request_id  - request_id object received through HttpRequest
    *
    * @return object
    */
    public function approveStudentRemoval($request_id)
    {
        $demand = Demand::find($request_id);
        
        // approve request
        $demand->approved = 1;
        $demand->save();
        
        session()->flash('approved_removal', '');
        
        $student = Student::find($demand->student_id);
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just approved request for removal of '.$student->first_name.' '.$student->last_name.' from the '.$student->group->group_name.' group from school.'
        ]);
        
        return redirect()->route('director.student.removal_requests');   
    }
    
    /**
    * This method processes PUT (update) request, which first retrieves student removal request which needs to be rejected,
    * updates it, and creates activity which is displayed on director profile.
    *
    * @param $request_id  - request_id object received through HttpRequest
    *
    * @return object
    */
    public function rejectStudentRemoval($request_id)
    {
        $demand = Demand::find($request_id);
        
        $demand->approved = -1;
        $demand->save();
        
        session()->flash('rejected_removal', '');
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just rejected request for removal of '.$student->first_name.' '.$student->last_name.' from the '.$student->group->group_name.' group from school.'
        ]);
        
        return redirect()->route('director.student.removal_requests');
    }
    
    /**
    * This method displays user profile (director in this case) overview together with director's activities.
    *
    * @param $username - username received through HttpRequest
    *
    * @return object
    */
    public function showProfile($username)
    {
        $director = Director::whereUsername($username)->first();
        
        // retrieve activities to show
        $activities = Activity::where('director_id', $director->id)->paginate(5);
        
        return view('admin.profile_view', compact('director', 'activities'));
    }
    
    /**
    * This method displays form for director editing profile.
    * It retrieves director and sends it to the admin.profile_edit for display.
    *
    * @return object
    */
    public function showEditProfile()
    {
        $director = Director::find(Auth::user()->id);
        
        return view('admin.profile_edit', compact('director'));
    }
    
    /**
    * This method processes PUT (update) request which first retrieves director that needs to be updated, validates form and
    * and updates director in the database.
    * It also creates activity which is displayed on director profile.
    *
    * @param $request - request object received through HttpRequest
    *
    * @return object
    */
    public function editProfile(Request $request)
    {
        $director = Director::find(Auth::user()->id);
        
        // messages
        $this->validate($request, [
            'first_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'last_name' => ['required', 'regex:/^[a-zA-Z]+$/'],
            'username' => 'required',
            'email' => 'required',
            'id_card' => ['required', 'size:8', 'regex:/^[0-9]{7}[a-zA-Z]{1}$/'],
            'address' => 'required',
            'city' => 'required'
        ], [
            'first_name.required' => 'Please Enter Manager\'s First Name',
            'first_name.regex' => 'First Name Must Contain Characters Only',
            'last_name.required' => 'Please Enter Manager\'s Last Name',
            'last_name.regex' => 'Last Name Must Contain Characters Only',
            'username.required' => 'Please Enter Username',
            'email.required' => 'Please Enter Email',
            'id_card.required' => 'Please Enter ID Card',
            'id_card.size' => 'ID Card Contains 8 Characters',
            'id_card.regex' => 'ID format is 7 numbers followed by 1 letter. Example: 0123456A',
            'address.required' => 'Please Enter Address',
            'city.required' => 'Please Enter City'
        ]);
        
        $director->first_name = $request->first_name;
        $director->last_name = $request->last_name;
        $director->username = $request->username;
        $director->email = $request->email;
        $director->id_card = $request->id_card; 
        $director->address = $request->address;
        $director->city = $request->city;
        
        // edit director
        $director->save();
        
        session()->flash('profile_modified', '');
        
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just modified his profile.'
        ]);
        
        return redirect()->route('director.profile.show', ['username' => $director->username]);
    }
    
    /**
    * This method displays view which allows director to browse all users in the system (managers, lecturers, students).
    *
    * @return object
    */
    public function showAllProfiles()
    {
        $managers = Manager::all();
        
        return view('admin.all_profiles', compact('managers'));
    }
    
    /**
    * This method displays manager profile choosen by director.
    *
    * @param $username - username received through HttpRequest
    *
    * @return object
    */
    public function showManagerProfile($username)
    {
        $manager = Manager::whereUsername($username)->first();
        $activities = Activity::where('manager_id', $manager->id)->paginate(5);
        
        return view('manager.profile_view', compact('manager', 'activities'));
    }
    
    /**
    * This method displays lecturer profile choosen by director.
    *
    * @param $lecturer - lecturer object received through HttpRequest
    *
    * @return object
    */
    public function showLecturerProfile(Lecturer $lecturer)
    {
        return view('lecturer.profile_view', compact('lecturer'));
    }
    
    /**
    * This method displays student profile choosen by director.
    *
    * @param $student - student object received through HttpRequest
    *
    * @return object
    */
    public function showStudentProfile(Student $student)
    {
        return view('student.profile_view', compact('student'));
    }
    
    /**
    * This method is used as method for processing AJAX request and returning JSON object to the view for display.
    *
    * @param $id - id received through HttpRequest
    *
    * @return object
    */
    public function retrieveProfiles($id)
    {
        // managers
        if($id == 1)
        {
            $managers = Manager::all();
            
            return response()->json(['role' => 'Manager', 'managers' => $managers]);
        }
        // lecturers
        else if($id == 2)
        {
            $lecturers = Lecturer::all();
            
            return response()->json(['role' => 'Lecturer', 'lecturers' => $lecturers]);   
        }
        else if($id == 3)
        {
            $students = Student::all();
            
            return response()->json(['role' => 'Student', 'students' => $students]);
        }
    }
    
    /**
    * This method displays the director's screen with all active conversations.
    *
    * @return object
    */
    public function showConversations()
    {
        // distinct conversation id's
        $distinctConvos = DB::table('messages')->select('conversation_id')->groupBy('conversation_id')->get()->toArray();
        
        $userConvos = [];
        
        // store DISTINCT conversations so that on the page one conversation is shown once only
        foreach($distinctConvos as $key => $distinctConvo)
        {
            $userConvos[$key] = Conversation::find($distinctConvos[$key]->conversation_id);
        }
        
        return view('admin.inbox', compact('userConvos'));
    }
    
    /**
    * This method displays the selected conversation together with messages and pagination if applicable (15 messages are shown per page).
    *
    * @param $conversation - conversation object retrieved through HttpRequest
    *
    * @return object
    */
    public function showConversation(Conversation $conversation)
    {
        // show 15 messages per page
        $conversationMessages = $conversation->messages()->paginate(15);
        
        // retrieve elast message sent by manager
        $lastMsgSentByManager = Message::where('conversation_id', $conversation->conversation_id)->where('sent_by', 0)->orderBy('created_at', 'desc')->first();
        
        return view('admin.conversation', compact('conversation', 'conversationMessages', 'lastMsgSentByManager'));
    }
    
    /**
    * This method is used to process POST request, validate the text and send message to the other participant.  
    *
    * @param $conversation - conversation object retrieved through HttpRequest
    * @param $request - request object retrieved through HttpRequest
    *
    * @return object
    */
    public function sendMessage(Conversation $conversation, Request $request) 
    {
        $this->validate($request, [
            'reply_msg' => 'required' 
        ], [
            'reply_msg.required' => 'Please enter the message.'
        ]);
        
        // find the manager in conversation
        $manager = Manager::find($conversation->messages[0]->manager_id);
        
        // store msg's sent by director
        Message::create([
            'conversation_id' => $conversation->conversation_id,
            'director_id' => Auth::user()->id,
            'manager_id' => $manager->id,
            'message_content' => $request->reply_msg,
            // sent by director
            'sent_by' => 1    
        ]);
        
        // create new activity
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just sent new message to '.$manager->first_name.' '.$manager->last_name.'.'
        ]);
        
        session()->flash('sent_msg', '');
        
        return redirect()->back();
    }
    
    /**
    * This method displays the director's screen for creating new conversation (only with managers with which he doesn't have conversation already).
    *
    * @return object
    */
    public function showNewConversationForm()
    {
        $managersNotInConvo = Manager::where('in_convo', 0)->get();
        $distinctConvos = DB::table('messages')->select('conversation_id')->groupBy('conversation_id')->get()->toArray();
        
        $userConvos = [];
        foreach($distinctConvos as $key => $distinctConvo)
        {
            $userConvos[$key] = Conversation::find($distinctConvos[$key]->conversation_id);
        }
        
        return view('admin.new_conversation', compact('managersNotInConvo', 'userConvos'));
    }
    
    /**
    * This method is used to process POST request, validate data (conversation title, message), start new conversation and send message. 
    *
    * @param $request - request object retrieved through HttpRequest
    *
    * @return object
    */
    public function startNewConversation(Request $request)
    {
        $this->validate($request, [
            'conv_title' => 'required|max:50',
            'message' => 'required'
        ], [
            'conv_title.required' => 'Please enter conversation title',
            'conv_title.max' => 'Conversation title can\'t have more than 50 characters.',
            'message.required' => 'Please enter message.'
        ]);
        
        // start new conversation
        $conversation = Conversation::create([
            'conversation_title' => $request->conv_title,
            'started_convo' => 1
        ]);
        
        // new message to db
        $msg = Message::create([
            'conversation_id' => $conversation->conversation_id,
            'director_id' => Auth::user()->id,
            'manager_id' => $request->manager,
            'message_content' => $request->message,
            'sent_by' => 1 
        ]);
        
        // update manager to indicate that he is in conversation
        $manager = Manager::find($request->manager);
        $manager->in_convo = 1;
        $manager->save();
        
        // create new activity for profile overview
        Activity::create([
            'director_id' => Auth::user()->id,
            'activity_content' => Auth::user()->first_name.' '.Auth::user()->last_name.' just started new conversation and sent message to '.$manager->first_name.' '.$manager->last_name.'.'
        ]);
        
        return redirect()->route('director.show.conversation', ['conversation' => $conversation]);
    }
}
