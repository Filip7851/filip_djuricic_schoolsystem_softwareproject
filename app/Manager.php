<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Manager extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
        'department_id',
        'first_name',
        'last_name',
        'email',
        'id_card',
        'username',
        'password',
        'in_convo'
    ];

    protected $hidden = [
        'password'
    ];
    
    protected $primaryKey = 'id';
    protected $guard = 'manager';
    
    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }
    
    public function demands() 
    {
        return $this->hasMany('App\Demand', 'manager_id');    
    }
    
    public function activities()
    {
        return $this->hasMany('App\Activity', 'manager_id');
    }
    
    public function messages()
    {
        return $this->hasMany('App\Message', 'manager_id');
    }
}
