<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Director extends Authenticatable
{
    use Notifiable;
    
    protected $guard = 'director';
    
    public function messages()
    {
        return $this->hasMany('App\Message', 'director_id', 'id');
    }
}
