<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Department;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\Schema::defaultStringLength(191);
        
        view()->composer(['manager.index', 'manager.add_student', 'manager.add_group', 'manager.make_request', 'manager.display_requests', 'manager.read_more_request', 'manager.browse_classes', 'manager.browse_class', 'manager.add_lecturer', 'manager.browse_lecturers', 'manager.remove_student', 'manager.display_removal_requests', 'manager.profile_edit', 'manager.inbox'], function($view) {
            $view->with('man_dept', Auth::user()->department->department_name);
        });
        
        view()->composer(['lecturer.index', 'student.index'], function($view) {
            $view->with('lec_dept', Department::where('department_id', Auth::user()->department_id)->first()->department_name);
        });     
        
        view()->composer('manager.add_group', function($view) {
            $view->with('man_dept_short', Department::where('department_id', Auth::user()->department_id)->first()->department_short);
        });
        
        // class count
        view()->composer(['manager.add_student', 'manager.browse_classes', 'manager.browse_class'], function($view) {
            $view->with('class_count_by_dept', DB::table('groups')->where('department_id', '=', Auth::user()->department_id)->count());
            $view->with('groups', DB::table('groups')->where('department_id', '=', Auth::user()->department_id)->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
