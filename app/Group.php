<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $primaryKey = 'group_id';
    
    protected $fillable = [
        'group_id',
        'department_id',
        'group_name',
        'level',
    ];
    
    public function students()
    {
        return $this->hasMany('App\Student', 'group_id');
    }
}
