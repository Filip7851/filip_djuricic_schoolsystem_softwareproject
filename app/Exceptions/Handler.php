<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);   
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        
        // Unauthenticated
        $guard = array_get($exception->guards(), 0);
                
        switch ($guard) {
            case 'director':
                if(Auth::guard('manager')->check())
                {
                    $login = 'manager.dashboard';   
                }
                else if(Auth::guard('lecturer')->check())
                {
                    $login = 'lecturer.dashboard';
                }
                else if(Auth::guard('student')->check())
                {
                    $login = 'student.dashboard';
                }
                else 
                {
                    $login = 'director.show';
                }
                break;  
            case 'manager':
                if(Auth::guard('director')->check())
                {
                    $login = 'director.dashboard';   
                }
                else if(Auth::guard('lecturer')->check())
                {
                    $login = 'lecturer.dashboard';
                }
                else if(Auth::guard('student')->check())
                {
                    $login = 'student.dashboard';
                }
                else 
                {
                    $login = 'manager.show';
                }
                break;
            // others here
            case 'lecturer':
                if(Auth::guard('director')->check())
                {
                    $login = 'director.dashboard';
                }
                else if(Auth::guard('manager')->check())
                {
                    $login = 'manager.dashboard';
                }
                else if(Auth::guard('student')->check())
                {
                    $login = 'student.dashboard';
                }
                else
                {
                    $login = 'lecturer.show';   
                }
                break;
            case 'student':
                if(Auth::guard('director')->check())
                {
                    $login = 'director.dashboard';
                }
                else if(Auth::guard('manager')->check())
                {
                    $login = 'manager.dashboard';
                }
                else if(Auth::guard('lecturer')->check())
                {
                    $login = 'lecturer.dashboard';
                }
                else
                {
                    $login = 'student.show';   
                }
                break;
            default:
                $login = 'login.show';
                break;
        }
    
        session()->flash('unauthorized', '');
        return redirect()->guest(route($login));
    }
}
