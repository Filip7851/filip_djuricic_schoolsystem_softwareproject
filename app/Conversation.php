<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = [
        'conversation_title',
        'started_convo'
    ];
    
    protected $primaryKey = 'conversation_id';
    
    public function messages()
    {
        return $this->hasMany('App\Message', 'conversation_id');
    }
}
