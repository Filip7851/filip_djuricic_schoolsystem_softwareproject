<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $primaryKey = 'department_id';
    public $timestamps = false;
    
    public function managers()
    {
        return $this->hasMany('App\Manager', 'department_id');
    }
}
